from collections import deque
from typing import Optional, Tuple

from geometry import square_neighbours, manhattan_dist_to_square as dist_to_square
from model.entity import Entity
from model.entity_type import EntityType


def is_free(point: Tuple[int, int], manager, ignore_units: bool, avoid_turrets: bool, through_resources=False) -> bool:
    if not manager.sight[point[0] * 80 + point[1]]:
        return False

    if avoid_turrets and manager.turret_range[point[0] * 80 + point[1]] > 0:
        return False

    cell = manager.map[point[0]][point[1]]
    if cell is None:
        return True

    if ignore_units and cell.is_unit:
        return True

    if through_resources and cell.entity_type == EntityType.RESOURCE:
        return True

    return False


def pathfind(manager, square, collector):
    sn = set(c for c in square_neighbours(square, manager.z) if manager.map[c[0]][c[1]] is None)
    queue = deque()
    visited = set()

    queue.append(collector)
    visited.add(collector[0] * 80 + collector[1])

    while len(queue) > 0:
        if len(queue) > 300:
            return None

        node = queue.popleft()
        if node in sn:
            return node

        for neighbour in manager.graph[node[0]][node[1]]:
            nsk = neighbour[0] * 80 + neighbour[1]
            if nsk in visited:
                continue

            if not is_free(neighbour, manager, True, True):
                continue

            visited.add(nsk)
            queue.append(neighbour)

    return None


def free_pos_around(building: Entity, manager, collector: Optional[Tuple[int, int]], use_pathfinding=True) \
        -> Optional[Tuple[int, int]]:
    if not collector:
        collector = (13, 13)

    square = building.as_square(manager.ep)

    if use_pathfinding:
        p = pathfind(manager, square, collector)
        if p:
            return p
    return min(
        (c for c in square_neighbours(square, manager.z) if manager.map[c[0]][c[1]] is None),
        key=lambda c: dist_to_square(c, square),
        default=None
    )


def attack_priority(entity):
    if not entity.is_unit and not entity.active:
        return 1
    elif entity.health <= 9:
        return 2
    elif entity.entity_type == EntityType.RANGED_UNIT:
        return 3
    elif entity.entity_type == EntityType.MELEE_UNIT:
        return 4
    elif entity.entity_type == EntityType.TURRET:
        return 5
    elif entity.entity_type == EntityType.BUILDER_UNIT:
        return 6
    elif entity.entity_type == EntityType.BUILDER_BASE:
        return 7
    elif entity.entity_type == EntityType.RANGED_BASE:
        return 8
    elif entity.entity_type == EntityType.MELEE_BASE:
        return 9
    elif entity.entity_type == EntityType.HOUSE:
        return 10
    elif entity.entity_type == EntityType.WALL:
        return 100
