from typing import Dict, Optional

import utils
from geometry import manhattan_dist_to_square as dist_to_square, manhattan_dist as dist
from model.build_action import BuildAction
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.vec2_int import Vec2Int
from . import Strat


class BuildCombatants(Strat):

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:
        # delta_food = self.manager.food_provided - self.manager.food_used
        # if delta_food < 1:
        #     return None

        if len(self.manager.my_buildings[EntityType.BUILDER_BASE]) and len(self.manager.my_builders) < 15 \
           and len(self.manager.saboteurs) == 0:
            return None

        r = self.manager.me.resource + self.manager.potential_resources
        result = {}
        collector = self.manager.middle_of_army
        if len(self.manager.saboteurs):
            nearest_saboteur = min(self.manager.saboteurs, key=lambda e: dist(e.position.as_tuple, (0, 0)))
            collector = nearest_saboteur.position.as_tuple

        if self.cost(EntityType.RANGED_UNIT) < 100:
            for entity in sorted(
                    self.manager.my_buildings[EntityType.RANGED_BASE],
                    key=lambda e: dist_to_square(collector, e.as_square(self.manager.ep))):
                # if delta_food < 1:
                #     break

                c = self.manager.middle_of_army
                if len(self.manager.saboteurs):
                    nearest_saboteur = min(
                        self.manager.saboteurs,
                        key=lambda e: dist_to_square(e.position.as_tuple, entity.as_square(self.manager.ep))
                    )
                    c = nearest_saboteur.position.as_tuple

                if (r - self.strategy.build_on_this_tick) >= self.strategy.cost(EntityType.RANGED_UNIT):
                    pos = utils.free_pos_around(entity, self.manager, c)
                    if pos:
                        self.strategy.build_on_this_tick += self.strategy.cost(EntityType.RANGED_UNIT)
                        self.strategy.build_on_this_tick_types[EntityType.RANGED_UNIT] += 1
                        # delta_food -= self.manager.ep[EntityType.RANGED_UNIT].population_use
                        result[entity.id] = EntityAction(build_action=BuildAction(EntityType.RANGED_UNIT, Vec2Int(*pos)))

        if self.strategy.cost(EntityType.MELEE_UNIT) > self.strategy.cost(EntityType.RANGED_UNIT) / 2.5 and \
                len(list(filter(lambda e: e.active, self.manager.my_buildings[EntityType.RANGED_BASE]))) > 0:
            return result

        for entity in sorted(
                self.manager.my_buildings[EntityType.MELEE_BASE],
                key=lambda e: dist_to_square(collector, e.as_square(self.manager.ep))):
            # if delta_food < 1:
            #     break

            c = self.manager.middle_of_army
            if len(self.manager.saboteurs):
                nearest_saboteur = min(
                    self.manager.saboteurs,
                    key=lambda e: dist_to_square(e.position.as_tuple, entity.as_square(self.manager.ep))
                )
                c = nearest_saboteur.position.as_tuple

            if (r - self.strategy.build_on_this_tick) >= self.strategy.cost(EntityType.MELEE_UNIT):
                pos = utils.free_pos_around(entity, self.manager, c)
                if pos:
                    self.strategy.build_on_this_tick += self.strategy.cost(EntityType.MELEE_UNIT)
                    self.strategy.build_on_this_tick_types[EntityType.MELEE_UNIT] += 1
                    # delta_food -= self.manager.ep[EntityType.MELEE_UNIT].population_use
                    result[entity.id] = EntityAction(build_action=BuildAction(EntityType.MELEE_UNIT, Vec2Int(*pos)))

        return result
