from typing import Dict, Optional

import consts
from geometry import manhattan_dist as dist
from model.attack_action import AttackAction
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.vec2_int import Vec2Int
from . import Strat


class Miners(Strat):

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:
        result = {}
        for entity in self.manager.my_builders:
            pos = entity.position.as_tuple
            x, y = pos

            neighbours = sorted(
                self.manager.graph[x][y],
                key=lambda p: dist(p, (0, 0)),
                reverse=True
            )
            resource = None
            for n in neighbours:
                cell = self.manager.map[n[0]][n[1]]
                if cell and cell.entity_type == EntityType.RESOURCE:
                    resource = cell
                    break

            if resource:
                result[entity.id] = EntityAction(attack_action=AttackAction(resource.id, None))
                continue

            if len(self.manager.miners_targets) > 0:
                move_target = self.manager.miners_targets[entity.id % len(self.manager.miners_targets)]
                x, y = pos
                while (x, y) != move_target and self.manager.sight[x*80+y]:
                    if move_target[0] > x:
                        x += 1
                    elif move_target[0] < x:
                        x -= 1
                    if move_target[1] > y:
                        y += 1
                    elif move_target[1] < y:
                        y -= 1
                move_target = (x, y)
            else:
                move_target = self.manager.free_resource(pos)
                if not move_target:
                    if len(self.manager.saboteurs):
                        nearest_saboteur = min(
                            self.manager.saboteurs,
                            key=lambda s: dist(s.position.as_tuple, pos)
                        )
                        move_target = nearest_saboteur.position.as_tuple
                    else:
                        move_target = (self.manager.z // 2, self.manager.z // 2)

            result[entity.id] = EntityAction(
                move_action=MoveAction(Vec2Int(*move_target), True, True),
                attack_action=consts.ATTACK_ACTION_BUILDERS,
            )

        return result
