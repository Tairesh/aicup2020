from typing import Dict, Optional, Tuple

from pathfinding import path_exists
from consts import BASE_ENTITIES, HOUSES
from geometry import manhattan_dist as dist, manhattan_dist_to_square as dist_to_square, \
    squares_around_point, cells_between, square_neighbours
from model.build_action import BuildAction
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.vec2_int import Vec2Int
from strats import Strat


class BuildBuildings(Strat):

    def is_square_free(self, square: Tuple[int, int, int], wo_units=False) -> bool:
        left, bottom, size = square
        if left < 0 or (left + size) > (self.manager.z - 1) or bottom < 0 or (bottom + size) > (self.manager.z - 1):
            return False

        for i in range(left, left + size):
            for j in range(bottom, bottom + size):
                cell = self.manager.map[i][j]
                if cell is not None:
                    if wo_units:
                        if not cell.is_unit:
                            return False
                    else:
                        return False
        return True

    def _is_square_available_for_build(self, square: Tuple[int, int, int], strict=True) -> bool:
        left, bottom, size = square
        right, top = left + size - 1, bottom + size - 1
        square_free = self.is_square_free(square)

        if not square_free:
            return False

        for i in range(left, right+1):
            for j in range(bottom, top+1):
                if self.manager.max_damage[i*80+j] > 0:
                    return False

        if any(1 for cx, cy in cells_between((left, bottom), (right, top)) if not self.manager.sight[cx*80+cy]):
            return False

        for square in self.strategy.assigned_as_builders.values():
            cells = cells_between((left, bottom), (right, top), self.manager.z)
            for cell in cells:
                if dist_to_square(cell, square) == 0:
                    return False

        if not strict:
            return True

        if left == 1 or bottom == 1:
            return False

        if left == 0 or bottom == 0:
            return not (left > 0 and right < 6 and bottom == 0)

        if not self.manager.f:
            if (right > 10 and left < 14 and top > 6 and bottom < 20) or \
                    (top > 10 and bottom < 14 and right > 6 and left < 20) or \
                    (right > 10 and left < 20 and 10 < top and bottom < 20):
                return False

        left_line = cells_between((left - 1, bottom), (left - 1, top), self.manager.z)
        right_line = cells_between((right + 1, bottom), (right + 1, top), self.manager.z)
        top_line = cells_between((left, top + 1), (right, top + 1), self.manager.z)
        bottom_line = cells_between((left, bottom - 1), (right, bottom - 1), self.manager.z)

        def is_free(pos: Tuple[int, int]):
            c = self.manager.map[pos[0]][pos[1]]
            if not c:
                return True
            if c.is_unit:
                return True
            if c.entity_type == EntityType.RESOURCE:
                return True
            return False

        ll_free = not any(1 for c in left_line if not is_free(c))
        rl_free = not any(1 for c in right_line if not is_free(c))
        tl_free = not any(1 for c in top_line if not is_free(c))
        bl_free = not any(1 for c in bottom_line if not is_free(c))
        lines_free = 0
        if ll_free:
            lines_free += 1
        if rl_free:
            lines_free += 1
        if bl_free:
            lines_free += 1
        if tl_free:
            lines_free += 1

        return lines_free == 4

    def _free_square_around_point(self, point: Tuple[int, int], square_size: int, strict=True) \
            -> Optional[Tuple[int, int, int]]:
        squares = squares_around_point(point, square_size)
        return min(
            filter(
                lambda s: self._is_square_available_for_build(s, strict),
                squares
            ),
            key=lambda s: s[0] + s[1],
            default=None,
        )

    def _free_square_for_house(self) -> Optional[Tuple[int, int, int]]:
        repairers_min_count = 1
        for x, y in HOUSES:
            square = (x, y, 3)
            if self._is_square_available_for_build(square, False):
                repairers_around = len([1 for e in self.manager.my_builders
                                        if dist_to_square(e.position.as_tuple, square) < 15])
                if repairers_around >= repairers_min_count:
                    return square
        return None

    def _free_square_on_base(self, square_size: int) -> Optional[Tuple[int, int, int]]:
        x, y = 0, 0
        base_top, base_right = self.manager.base_corner
        repairers_min_count = 1 if square_size < 5 else 4

        while x < base_right and y < base_top:
            square = (x, y, square_size)
            if self._is_square_available_for_build(square):
                repairers_around = len(list(filter(
                    lambda e: dist_to_square(e.position.as_tuple, square) < 15,
                    self.manager.my_builders,
                )))
                if repairers_around >= repairers_min_count:
                    return square
            if x == base_right - 1:
                x = 0
                y += 1
            else:
                x += 1
        return None

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:

        mb = self.manager.my_buildings
        myid = self.manager.me.id
        # my_army = len(self.manager.my_combatants)
        count_bb = len(mb[EntityType.BUILDER_BASE])
        # count_active_bb = len([1 for i in mb[EntityType.BUILDER_BASE] if i.active])
        count_rb = len(mb[EntityType.RANGED_BASE])
        count_active_rb = len([1 for i in mb[EntityType.RANGED_BASE] if i.active])
        count_mb = len(mb[EntityType.MELEE_BASE])
        # count_active_mb = len([1 for i in mb[EntityType.MELEE_BASE] if i.active])
        delta_food = self.manager.food_provided_potential - self.manager.food_used
        food_reserve = 10
        r = self.manager.me.resource + self.manager.potential_resources
        cost = self.strategy.cost
        initial_food_reserve = {
            1: 0,
            2: 25,
            3: 50,
        }[self.manager.round]

        need_to_build = [
            (EntityType.BUILDER_BASE, count_bb == 0),
            (EntityType.RANGED_BASE, count_rb == 0),
            (EntityType.MELEE_BASE, count_mb == 0 and count_rb > 0 and self.manager.seen_any_turret),
            (EntityType.HOUSE, delta_food < food_reserve and count_bb > 0 and
             (count_rb > 0 or self.manager.food_provided < initial_food_reserve or
              r >= cost(EntityType.RANGED_BASE) + cost(EntityType.HOUSE))),
            (EntityType.TURRET, count_rb > 0 and
             (count_active_rb == 0 or cost(EntityType.RANGED_UNIT) > 60 or
              (r - self.strategy.build_on_this_tick) >= (cost(EntityType.RANGED_UNIT) + cost(EntityType.TURRET)))),
        ]
        builders = sorted([
            b for b in self.manager.my_builders
            if not any(dist(s.position.as_tuple, b.position.as_tuple) <= 11 for s in self.manager.saboteurs)
           ], key=lambda e: e.position.x + e.position.y)
        result = {}

        for typ in (t for t, f in need_to_build if f):
            if r < self.strategy.cost(typ):
                continue

            props = self.manager.ep[typ]
            builder_and_square = None

            for bid, square in self.strategy.assigned_as_builders_prev.items():
                builder = self.manager.entities[bid] if bid in self.manager.entities else None

                if square[2] == props.size and builder \
                        and self._is_square_available_for_build(square, False) \
                        and path_exists(self.manager, builder.position.as_tuple, (square[0] + 1, square[1] + 1),
                                        ignore_units=True, use_neighbour_point=True, avoid_turrets=True):
                    builder_and_square = builder, square
                    break

            if not builder_and_square and typ != EntityType.HOUSE:
                free_builders = sorted(
                    filter(lambda e: e.id not in self.strategy.assigned_as_builders, builders),
                    key=lambda e: e.position.x + e.position.y,
                )

                for builder in free_builders:

                    pos = builder.position.as_tuple
                    x, y = pos
                    in_line = True

                    if typ == EntityType.TURRET:
                        if x >= self.manager.z - 20 or y >= self.manager.z - 20:
                            in_line = False
                        elif x < 25 and y < 25:
                            in_line = False
                        else:
                            if x < 30:
                                rect = 0, y+1, self.manager.z-1, y+1
                            elif y < 30:
                                rect = x+1, 0, x+1, self.manager.z-1
                            else:
                                rect = x+1, y+1, self.manager.z-1, self.manager.z-1

                            for i in range(rect[0], rect[2]):
                                for j in range(rect[1], rect[3]):
                                    cell = self.manager.map[i][j]
                                    if cell and cell.player_id == myid and cell.entity_type in BASE_ENTITIES:
                                        in_line = False
                                        break
                                if not in_line:
                                    break
                            if in_line:
                                count_turrets_around = len([1 for e in self.manager.my_buildings[EntityType.TURRET]
                                                            if dist_to_square(pos, e.as_square(self.manager.ep)) < 10])
                                if count_turrets_around > 3:
                                    in_line = False

                    if not in_line:
                        # print(x, y, "not in line")
                        continue

                    square = self._free_square_around_point(pos, props.size, typ != EntityType.TURRET)
                    if square:
                        repairers_around = len(list(filter(
                            lambda e: dist_to_square(e.position.as_tuple, square) < 5,
                            free_builders,
                        )))
                        if repairers_around > 2:
                            builder_and_square = builder, square
                            # print(f"square around found {square}")
                            break

            if not builder_and_square and typ == EntityType.HOUSE:
                square = self._free_square_for_house()
                if square:
                    builder = min(
                        filter(lambda e: e.id not in self.strategy.assigned_as_builders, builders),
                        key=lambda e: dist_to_square(e.position.as_tuple, square),
                        default=None,
                    )
                    if builder and path_exists(self.manager, builder.position.as_tuple, (square[0] + 1, square[1] + 1),
                                               avoid_turrets=True):
                        builder_and_square = builder, square

            if not builder_and_square and typ != EntityType.TURRET and typ != EntityType.HOUSE:
                # print("square around not found!")
                square = self._free_square_on_base(props.size)
                if square:
                    builder = min(
                        filter(lambda e: e.id not in self.strategy.assigned_as_builders, builders),
                        key=lambda e: dist_to_square(e.position.as_tuple, square),
                        default=None,
                    )
                    if builder and path_exists(self.manager, builder.position.as_tuple, (square[0] + 1, square[1] + 1),
                                               avoid_turrets=True):
                        builder_and_square = builder, square
                        # print(f"square on base found {square}")
                else:
                    # print("square on base not found!")
                    pass

            if builder_and_square:
                builder, square = builder_and_square
                left, bottom, size = square
                result[builder.id] = EntityAction(
                    move_action=MoveAction(Vec2Int(left + size // 2, bottom + size // 2), True, True),
                    build_action=BuildAction(typ, Vec2Int(left, bottom)),
                )
                if builder.position.as_tuple in square_neighbours(square, self.manager.z):
                    self.strategy.build_on_this_tick += self.strategy.cost(typ)
                    self.strategy.build_on_this_tick_types[typ] += 1
                    self.strategy.assigned_as_builders[builder.id] = square

        return result
