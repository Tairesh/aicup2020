from typing import Optional, Dict, Tuple

from collections import deque

from numpy.ma import floor

from model.attack_action import AttackAction
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.repair_action import RepairAction
from model.vec2_int import Vec2Int
from strats import Strat
from geometry import manhattan_dist as dist
from utils import is_free


class Runaway(Strat):

    def pos_to_run(self, miner, strict=True) -> Optional[Tuple[int, int]]:
        pos = miner.position.as_tuple
        queue = deque()
        visited = set()

        queue.append(pos)
        visited.add(pos)

        while len(queue) > 0:
            if len(queue) > 30:
                return None

            node = queue.popleft()
            if self.get_max_damage_around(node) == 0:
                return node

            if not strict and self.manager.max_damage[node[0]*80 + node[1]] == 0:
                return node

            for neighbour in self.manager.graph[node[0]][node[1]]:

                if neighbour in visited:
                    continue

                if not is_free(neighbour, self.manager, True, False):
                    continue

                visited.add(neighbour)
                queue.append(neighbour)
        return None

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:
        result = {}
        turret_max_health = self.manager.ep[EntityType.TURRET].max_health
        turret_damage = self.manager.ep[EntityType.TURRET].attack.damage
        for miner in self.manager.my_builders:
            pos = miner.position.as_tuple
            dmg_around = self.get_max_damage_around(pos)
            if dmg_around:
                if self.manager.max_damage[pos[0]*80+pos[1]] == 0:
                    turret_around = None
                    resource_around = None
                    for nx, ny in self.manager.graph[pos[0]][pos[1]]:
                        cell = self.manager.map[nx][ny]
                        if cell:
                            if cell.entity_type == EntityType.TURRET and \
                              cell.player_id == self.manager.me.id and cell.health < turret_max_health:
                                turret_around = cell
                            if cell.entity_type == EntityType.RESOURCE:
                                resource_around = cell

                    if turret_around:
                        result[miner.id] = EntityAction(repair_action=RepairAction(turret_around.id))
                        continue
                    if resource_around:
                        enemy_turrets_damage = 0

                        for nx, ny in self.manager.graph[pos[0]][pos[1]]:
                            if self.manager.turret_range[nx*80 + ny]:
                                enemy_turrets_damage += turret_damage
                        if dmg_around <= enemy_turrets_damage:
                            result[miner.id] = EntityAction(attack_action=AttackAction(resource_around.id, None))

                run_pos = self.pos_to_run(miner, True)
                if run_pos is None:
                    run_pos = self.pos_to_run(miner, False)
                if run_pos is None:
                    run_pos = min(
                        (p for p in self.manager.graph[pos[0]][pos[1]] if self.manager.map[p[0]][p[1]] is None),
                        key=lambda p: self.get_max_damage_around(p),
                        default=None,
                    )
                if run_pos:
                    result[miner.id] = EntityAction(move_action=MoveAction(Vec2Int(*run_pos), True, True))

        for entity in self.manager.my_combatants:
            if entity.entity_type == EntityType.RANGED_UNIT:
                pos = entity.position.as_tuple
                x, y = pos
                melees = self.get_meeles_objects_around(pos)
                danger = len(melees)
                if danger > 0:
                    ma = self.manager.ep[EntityType.MELEE_UNIT].attack.damage
                    ra = self.manager.ep[EntityType.RANGED_UNIT].attack.damage
                    sum_health = sum(e.health for e in melees)
                    ticks_to_win = floor(sum_health/ra + 0.999)
                    h = entity.health - (ma * danger * ticks_to_win)
                    if h > 0:
                        continue

                    free_neighbours = set(filter(
                        lambda p: self.manager.map[p[0]][p[1]] is None,
                        self.manager.graph[x][y],
                    ))
                    if len(free_neighbours):
                        min_melees_around = min((self.get_meeles_around(p) for p in free_neighbours))
                        run_pos = min(
                            filter(
                                lambda p: self.get_meeles_around(p) <= min_melees_around,
                                free_neighbours
                            ),
                            key=lambda p: dist(p, (13, 13))
                        )
                        if run_pos and self.get_meeles_around(run_pos) < self.get_meeles_around(pos):
                            result[entity.id] = EntityAction(move_action=MoveAction(Vec2Int(*run_pos), False, False))

        return result
