from typing import Dict, Optional

from consts import COEFF_MINERS_TO_RES
from geometry import manhattan_dist
from model.build_action import BuildAction
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.vec2_int import Vec2Int
from strats import Strat
from utils import free_pos_around


class BuildBuilders(Strat):

    def need_to_build(self) -> bool:
        mb = self.manager.my_buildings
        count_active_mb = len([1 for i in mb[EntityType.MELEE_BASE] if i.active])
        count_active_rb = len([1 for i in mb[EntityType.RANGED_BASE] if i.active])

        if count_active_mb == 0 and count_active_rb == 0:
            return True

        army = len(self.manager.my_combatants)
        builders = len(self.manager.my_builders)
        if army > 10 and builders > army * 2:
            return False

        resources_factor = max(30, self.manager.resources_count*3) if self.manager.player_view.fog_of_war else \
            self.manager.resources_count * (1 / (4 * COEFF_MINERS_TO_RES))
        if len(self.manager.my_builders) > resources_factor:
            return False

        res = self.manager.me.resource + self.manager.potential_resources \
            - self.strategy.build_on_this_tick - self.cost(EntityType.BUILDER_UNIT)
        delta_food = self.manager.food_provided - self.manager.food_used
        if delta_food < 5 and self.cost(EntityType.HOUSE) > res and len(self.manager.my_builders) > 4:
            # don't build builders if need to save for house
            return False

        return True

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:

        if not self.need_to_build():
            return None

        result = {}
        for entity in self.manager.my_buildings[EntityType.BUILDER_BASE]:
            left_bottom = entity.position.as_tuple
            if any(manhattan_dist(s.position.as_tuple, left_bottom) < 30 for s in self.manager.saboteurs):
                continue

            collector = self.manager.free_resource(left_bottom)
            if not collector:
                collector = entity.position.x + 5, entity.position.y + 5
            pos = free_pos_around(entity, self.manager, collector)
            if pos:
                self.strategy.build_on_this_tick += self.cost(EntityType.BUILDER_UNIT)
                self.strategy.build_on_this_tick_types[EntityType.BUILDER_UNIT] += 1
                result[entity.id] = EntityAction(build_action=BuildAction(EntityType.BUILDER_UNIT, Vec2Int(*pos)))

        return result
