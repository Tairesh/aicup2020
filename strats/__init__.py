from abc import ABC, abstractmethod
from typing import Dict, Optional, Tuple, List

from model.entity import Entity
from model.entity_action import EntityAction
from model.entity_type import EntityType


class Strat(ABC):

    def __init__(self, strategy):
        from my_strategy import MyStrategy
        self.strategy: MyStrategy = strategy

    @property
    def manager(self):
        return self.strategy.manager

    @abstractmethod
    def get_actions(self) -> Optional[Dict[int, EntityAction]]: pass

    def cost(self, typ: EntityType) -> int:
        return self.strategy.cost(typ)

    def get_turrets_range_around(self, point: Tuple[int, int]) -> int:
        dmg = 0
        for n in self.manager.graph[point[0]][point[1]]:
            nx, ny = n
            dmg += self.manager.turret_range[nx*self.manager.z + ny]

        return dmg

    def get_max_damage_around(self, point: Tuple[int, int]) -> int:
        dmg = 0
        for n in self.manager.graph[point[0]][point[1]]:
            nx, ny = n
            dmg += self.manager.max_damage[nx*self.manager.z + ny]

        return dmg

    def get_meeles_around(self, point: Tuple[int, int]) -> int:
        meeles = 0
        pos_neighbours = self.manager.graph[point[0]][point[1]]
        neighbours = set()
        for n in pos_neighbours:
            neighbours.add(n)
            neighbours |= self.manager.graph[n[0]][n[1]]
        neighbours.remove(point)

        for n in neighbours:
            enemy = self.manager.map[n[0]][n[1]]
            if enemy and enemy.entity_type == EntityType.MELEE_UNIT and enemy.player_id != self.manager.me.id:
                meeles += 1

        return meeles

    def get_meeles_objects_around(self, point: Tuple[int, int]) -> List[Entity]:
        meeles = []
        pos_neighbours = self.manager.graph[point[0]][point[1]]
        neighbours = set()
        for n in pos_neighbours:
            neighbours.add(n)
            neighbours |= self.manager.graph[n[0]][n[1]]
        neighbours.remove(point)

        for n in neighbours:
            enemy = self.manager.map[n[0]][n[1]]
            if enemy and enemy.entity_type == EntityType.MELEE_UNIT and enemy.player_id != self.manager.me.id:
                meeles.append(enemy)

        return meeles
