from typing import Optional, Dict

from model.attack_action import AttackAction
from model.auto_attack import AutoAttack
from model.entity_action import EntityAction
from model.entity_type import EntityType
from utils import attack_priority
from . import Strat
from geometry import cells_in_range, manhattan_dist_to_square as dist_to_square


class Turrets(Strat):
    def get_actions(self) -> Optional[Dict[int, EntityAction]]:
        result = {}
        m = self.manager.map
        ar = self.manager.ep[EntityType.TURRET].attack.attack_range

        for turret in self.manager.my_buildings[EntityType.TURRET]:
            at = self.strategy.attack_targets
            enemies = [
                m[cx][cy] for cx, cy in cells_in_range(turret.as_square(self.manager.ep), ar, self.manager.z)
                if m[cx][cy] and m[cx][cy].player_id and m[cx][cy].player_id != self.manager.me.id
            ]
            enemies_to_attack = [e for e in enemies if (e.id not in at or e.health > at[e.id])
                                 and dist_to_square(turret.position.as_tuple, e.as_square(self.manager.ep)) <= ar]
            min_attack_priority = min((attack_priority(e) for e in enemies_to_attack), default=0)
            enemy_in_site = min(
                (e for e in enemies_to_attack if attack_priority(e) <= min_attack_priority),
                key=lambda e: e.health,
                default=None,
            )
            if enemy_in_site:
                if enemy_in_site.id in at:
                    at[enemy_in_site.id] += self.manager.ep[EntityType.TURRET].attack.damage
                else:
                    at[enemy_in_site.id] = self.manager.ep[EntityType.TURRET].attack.damage
                result[turret.id] = EntityAction(attack_action=AttackAction(enemy_in_site.id, None))
            else:
                result[turret.id] = EntityAction(attack_action=AttackAction(None, AutoAttack(10, [])))

        return result
