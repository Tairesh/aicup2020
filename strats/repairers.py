from typing import Dict, Optional

from geometry import manhattan_dist as dist, manhattan_dist_to_square as dist_to_square, square_neighbours
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.repair_action import RepairAction
from model.vec2_int import Vec2Int
from pathfinding import distance
from . import Strat


class Repairers(Strat):

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:

        builders = sorted(self.manager.my_builders, key=lambda e: e.id)
        free_builders = sorted(
            filter(lambda e: e.id not in self.strategy.assigned_as_builders, builders),
            key=lambda e: e.position.x + e.position.y,
        )
        result = {}

        for builder in free_builders:
            x, y = builder.position.as_tuple
            for n in (self.manager.map[p[0]][p[1]] for p in self.manager.graph[x][y]):
                if n and n.player_id == self.manager.me.id \
                        and (not n.active or n.health <= self.manager.ep[n.entity_type].max_health // 2 or (
                        n.health < self.manager.ep[n.entity_type].max_health and n.entity_type == EntityType.TURRET)):
                    result[builder.id] = EntityAction(repair_action=RepairAction(n.id))
                    self.strategy.assigned_pos_to_repair.add((x, y))
                    self.strategy.assigned_as_builders[builder.id] = n.as_square(self.manager.ep)
                    break

        for building in self.manager.need_repair:
            square = building.as_square(self.manager.ep)
            props = self.manager.ep[building.entity_type]
            max_health = props.max_health if not building.active or building.entity_type == EntityType.TURRET\
                else props.max_health // 2

            neighbours = []
            count_repairers = 0
            for p in square_neighbours(square, self.manager.z):
                cell = self.manager.map[p[0]][p[1]]
                if cell and cell.entity_type == EntityType.BUILDER_UNIT and cell.player_id == self.manager.me.id:
                    count_repairers += 1
                if cell is None:
                    neighbours.append(p)

            if count_repairers > props.size:
                continue

            enemies_around = any(
                filter(
                    lambda e: dist_to_square(e.position.as_tuple, square) < 10,
                    self.manager.enemy_entities,
                )
            )
            if enemies_around and building.entity_type != EntityType.TURRET:
                continue

            free_builders = sorted(
                filter(lambda e: e.id not in self.strategy.assigned_as_builders, builders),
                key=lambda e: dist_to_square(e.position.as_tuple, square),
            )

            near_units = sorted(
                filter(
                    lambda e: dist_to_square(e.position.as_tuple, square) < 2*props.size,
                    free_builders,
                ),
                key=lambda e: dist_to_square(e.position.as_tuple, square)
            )
            if len(near_units) == 0:
                nearest_unit = min(
                    free_builders,
                    key=lambda e: dist_to_square(e.position.as_tuple, square),
                    default=None
                )
                if nearest_unit:
                    near_units.append(nearest_unit)

            for repairer in near_units[:props.size]:
                rpos = repairer.position.as_tuple
                if rpos in neighbours:
                    result[repairer.id] = EntityAction(repair_action=RepairAction(building.id))
                    continue

                if repairer.id in self.strategy.assigned_as_builders_prev:
                    prev_square = self.strategy.assigned_as_builders_prev[repairer.id]
                    cell = self.manager.map[prev_square[0]][prev_square[1]]
                    if cell and cell.id != building.id and cell.player_id == self.manager.me.id and \
                       cell.health < self.manager.ep[cell.entity_type].max_health:
                        continue

                positions = sorted(neighbours, key=lambda p: dist(p, rpos))
                for pos_to_repair in positions:
                    if pos_to_repair in self.strategy.assigned_pos_to_repair:
                        continue
                    d = distance(self.manager, rpos, pos_to_repair, avoid_turrets=True)
                    if d is not None:
                        future_hp = building.health + d*count_repairers
                        will_be_mined = d
                        if future_hp < max_health and (max_health-future_hp) > will_be_mined:
                            result[repairer.id] = EntityAction(
                                move_action=MoveAction(Vec2Int(*pos_to_repair), True, True)
                            )
                            self.strategy.assigned_pos_to_repair.add(pos_to_repair)
                            self.strategy.assigned_as_builders[repairer.id] = square
                            break

        return result
