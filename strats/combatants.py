from typing import Optional, Dict

from consts import BASE_ENTITIES
from geometry import manhattan_dist as dist, manhattan_dist_to_square as dist_to_square, cells_between
from model.attack_action import AttackAction
from model.entity import Entity
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.vec2_int import Vec2Int
from utils import attack_priority
from . import Strat


class Combatants(Strat):

    def get_combatant_strat(self, entity: Entity) -> EntityAction:
        pos = entity.position.as_tuple
        props = self.manager.ep[entity.entity_type]
        ar = props.attack.attack_range
        pta = self.strategy.players_to_attack
        at = self.strategy.attack_targets
        x, y = pos

        move_target = self.manager.middle_of_army
        if len(self.manager.my_combatants) > 2:
            if not self.manager.player3gone and self.manager.middle_of_army[0] >= self.manager.middle_of_army[1]:
                move_target = (self.manager.z - 5, 4)
            elif not self.manager.player4gone:
                move_target = (4, self.manager.z - 5)
            else:
                move_target = (self.manager.z - 5, self.manager.z - 5)
        break_through = True

        if len(self.manager.saboteurs):
            nearest_base = min(
                filter(
                    lambda e: e.entity_type in BASE_ENTITIES and e.entity_type != EntityType.TURRET
                    and dist_to_square(pos, e.as_square(self.manager.ep)) <= 20,
                    self.manager.enemy_entities,
                ),
                key=lambda e: dist_to_square(pos, e.as_square(self.manager.ep)),
                default=None,
            )
            if nearest_base:
                move_target = nearest_base.position.x + 1, nearest_base.position.y + 1
                break_through = True
            else:
                nearest_saboteur = min(
                    filter(
                        lambda s: s.id not in self.strategy.assigned_saboteurs_to_damage
                        or self.strategy.assigned_saboteurs_to_damage[s.id] < s.health
                        or dist(s.position.as_tuple, pos) < 8,
                        self.manager.saboteurs
                    ),
                    key=lambda s: dist(s.position.as_tuple, pos),
                    default=None
                )
                if not nearest_saboteur:
                    nearest_saboteur = min(
                        self.manager.saboteurs,
                        key=lambda s: dist(s.position.as_tuple, pos)
                    )

                if entity.health > self.manager.max_damage[x*self.manager.z+y]:
                    if nearest_saboteur in self.strategy.assigned_saboteurs_to_damage:
                        self.strategy.assigned_saboteurs_to_damage[nearest_saboteur.id] += props.attack.damage
                    else:
                        self.strategy.assigned_saboteurs_to_damage[nearest_saboteur.id] = props.attack.damage
                move_target = nearest_saboteur.position.as_tuple
                break_through = True
        elif len(pta):
            nearest_building = min(
                filter(
                    lambda e: e.player_id in pta,
                    self.manager.enemy_entities,
                ),
                key=lambda e: dist_to_square(pos, e.as_square(self.manager.ep)),
                default=None,
            )
            if nearest_building:
                move_target = (nearest_building.position.x + 1, nearest_building.position.y + 1)
                break_through = True
        elif entity.health <= props.max_health // 2:
            nearest_repairer = min(
                filter(
                    lambda e: e.id not in self.strategy.assigned_as_builders,
                    self.manager.my_builders,
                ),
                key=lambda e: dist(e.position.as_tuple, pos),
                default=None,
            )
            if nearest_repairer:
                self.strategy.assigned_as_builders[nearest_repairer.id] = (x, y, props.size)
                move_target = nearest_repairer.position.as_tuple
                break_through = False

        cells = cells_between((x - ar, y - ar), (x + ar, y + ar), self.manager.z)
        enemies = [self.manager.map[p[0]][p[1]] for p in cells
                   if self.manager.map[p[0]][p[1]] and self.manager.map[p[0]][p[1]].entity_type != EntityType.RESOURCE
                   and self.manager.map[p[0]][p[1]].player_id != self.manager.me.id]
        enemies_to_attack = [e for e in enemies if (e.id not in at or e.health > at[e.id])
                             and dist_to_square(pos, e.as_square(self.manager.ep)) <= ar]
        min_attack_priority = min((attack_priority(e) for e in enemies_to_attack), default=0)
        enemy_in_site = min(
            (e for e in enemies_to_attack if attack_priority(e) <= min_attack_priority),
            key=lambda e: e.health,
            default=None,
        )
        if enemy_in_site:
            if enemy_in_site.id in at:
                at[enemy_in_site.id] += props.attack.damage
            else:
                at[enemy_in_site.id] = props.attack.damage
            return EntityAction(attack_action=AttackAction(enemy_in_site.id, None))

        if move_target == pos:
            return EntityAction()

        if entity.entity_type == EntityType.RANGED_UNIT:
            r = self.get_turrets_range_around(pos)
            if r > 0:
                min_r = min(self.get_turrets_range_around(n) for n in self.manager.graph[x][y])
                if min_r < r:
                    move_target = min(
                        filter(
                            lambda n: self.get_turrets_range_around(n) <= min_r,
                            self.manager.graph[x][y]
                        ),
                        key=lambda n: dist(n, move_target)
                    )
        elif entity.entity_type == EntityType.MELEE_UNIT:
            enemies = (self.manager.map[cx][cy] for cx, cy in cells_between((x-5, y-5), (x+5, y+5))
                       if self.manager.map[cx][cy] and self.manager.map[cx][cy].player_id and
                       self.manager.map[cx][cy].player_id != self.manager.me.id)
            closest_enemy = min(
                enemies,
                key=lambda e: dist_to_square(pos, e.as_square(self.manager.ep)),
                default=None,
            )
            if closest_enemy and dist_to_square(pos, closest_enemy.as_square(self.manager.ep)) < 10:
                return EntityAction(move_action=MoveAction(closest_enemy.position, True, True))

        return EntityAction(
            move_action=MoveAction(Vec2Int(*move_target), True, break_through),
        )

    def get_actions(self) -> Optional[Dict[int, EntityAction]]:

        result = {}

        for combatant in self.manager.my_combatants:
            result[combatant.id] = self.get_combatant_strat(combatant)

        return result
