from model.client_message import DebugMessage, RequestDebugState
from model.debug_state import DebugState


class DebugInterface:
    def __init__(self, reader, writer):
        self.reader = reader
        self.writer = writer

    def send(self, command):
        DebugMessage(command).write_to(self.writer)
        self.writer.flush()

    def get_state(self):
        RequestDebugState().write_to(self.writer)
        self.writer.flush()
        return DebugState.read_from(self.reader)
