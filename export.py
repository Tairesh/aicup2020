#!/usr/bin/pypy3
import os
from zipfile import ZipFile

ZIP_NAME = 'dist/aicup-python.zip'

os.makedirs(os.path.dirname(ZIP_NAME), exist_ok=True)
if os.path.isfile(ZIP_NAME):
    os.remove(ZIP_NAME)

main_dir = os.path.dirname(os.path.abspath(__file__))
subdirs = ('model', 'strats')

with ZipFile(ZIP_NAME, 'w') as myzip:
    for file in os.listdir(main_dir):
        if file != 'export.py' and os.path.isfile(file) and file.endswith('.py'):
            myzip.write(file)
    for subdir in subdirs:
        for file in os.listdir(os.path.join(main_dir, subdir)):
            if os.path.isfile(os.path.join(main_dir, subdir, file)) and file.endswith('.py'):
                myzip.write(os.path.join(subdir, file))
