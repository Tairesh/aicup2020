from typing import Optional, Tuple, Dict, Set, List

import consts
import pathfinding
from debug_interface import DebugInterface
from geometry import manhattan_dist as dist, cells_in_range, line
from manager import Manager
from model.action import Action
from model.attack_action import AttackAction
from model.auto_attack import AutoAttack
from model.entity_action import EntityAction
from model.entity_type import EntityType
from model.move_action import MoveAction
from model.player_view import PlayerView
from model.vec2_int import Vec2Int
from strats import Strat
from strats.build_builders import BuildBuilders
from strats.build_buildings import BuildBuildings
from strats.build_combatants import BuildCombatants
from strats.combatants import Combatants
from strats.miners import Miners
from strats.repairers import Repairers
from strats.runaway import Runaway
from strats.turrets import Turrets
from utils import is_free


# from model.primitive_type import PrimitiveType
# from model.vec2_float import Vec2Float
# from model.color import Color, YELLOW, RED, GREEN
# from model.colored_vertex import ColoredVertex
# from model.debug_command import Add, Clear
# from model.debug_data import Primitives


class MyStrategy:

    def __init__(self):
        self.manager: Optional[Manager] = None
        self.strats: List[Strat] = [
            BuildBuilders(self),
            BuildCombatants(self),

            Miners(self),
            Repairers(self),
            BuildBuildings(self),

            Combatants(self),
            Turrets(self),
            Runaway(self),
        ]
        self.action: Action = Action({})

        self.build_on_this_tick = 0
        self.build_on_this_tick_types = {
            EntityType.WALL: 0,
            EntityType.HOUSE: 0,
            EntityType.BUILDER_BASE: 0,
            EntityType.BUILDER_UNIT: 0,
            EntityType.MELEE_BASE: 0,
            EntityType.MELEE_UNIT: 0,
            EntityType.RANGED_BASE: 0,
            EntityType.RANGED_UNIT: 0,
            EntityType.TURRET: 0,
        }

        self._players_to_attack: Optional[Set[int]] = None
        self.attack_targets: Dict[int, int] = {}
        self.assigned_saboteurs_to_damage: Dict[int, int] = {}
        self.assigned_pos_to_repair: Set[Tuple[int, int]] = set()
        self.assigned_as_builders_prev: Dict[int, Tuple[int, int, int]] = {}
        self.assigned_as_builders: Dict[int, Tuple[int, int, int]] = {}
        self.assigned_paths: Dict[int, List[Tuple[int, int]]] = {}

    def init_tick(self, player_view: PlayerView):
        if not self.manager:
            self.manager = Manager(player_view)

        self.manager.tick_update(player_view)

        self.build_on_this_tick = 0
        for k in self.build_on_this_tick_types:
            self.build_on_this_tick_types[k] = 0
        self._players_to_attack = None
        self.attack_targets.clear()
        self.assigned_saboteurs_to_damage.clear()
        self.assigned_pos_to_repair.clear()
        self.assigned_as_builders_prev = self.assigned_as_builders.copy()
        self.assigned_as_builders.clear()

    def cost(self, typ: EntityType) -> int:
        if typ == EntityType.BUILDER_UNIT:
            k = len(self.manager.my_builders)
        elif typ in consts.COMBATANTS:
            k = len([1 for e in self.manager.my_combatants if e.entity_type == typ])
        else:
            k = 0

        n = self.build_on_this_tick_types[typ] if typ in self.build_on_this_tick_types else 0

        return self.manager.ep[typ].initial_cost + k + n

    @property
    def players_to_attack(self) -> List[int]:
        if self._players_to_attack is None:
            self._players_to_attack = []
            pids = sorted([p.id for p in self.manager.player_view.players if p.id != self.manager.me.id],
                          key=lambda pid: abs(pid - self.manager.me.id),
                          reverse=True)
            my_army = len([1 for e in self.manager.my_combatants
                           if dist(e.position.as_tuple, self.manager.middle_of_army) < 10])
            for pid in pids:
                army = len([1 for e in self.manager.enemy_units[pid] if e.entity_type in consts.COMBATANTS])
                if (army < (my_army / 2) or army == 0) and (any(self.manager.enemy_buildings[pid])
                                                            or any(self.manager.enemy_units[pid])):
                    self._players_to_attack.append(pid)
                    my_army -= army

        return self._players_to_attack

    def get_action(self, player_view: PlayerView, debug_interface: DebugInterface = None) -> Action:
        self.init_tick(player_view)
        self.action = Action(dict((e.id, EntityAction()) for e in self.manager.my_entities))

        for strat in self.strats:
            strat_actions = strat.get_actions()
            if strat_actions:
                for entity_id, entity_action in strat_actions.items():
                    self.action.entity_actions[entity_id] = entity_action

        enl = len(self.manager.my_entities)
        army = len(self.manager.my_combatants)
        if enl < 200:
            for entity in self.manager.my_entities:
                if not entity.is_unit:
                    continue
                action = self.action.entity_actions[entity.id]
                if action.move_action:
                    pos = entity.position.as_tuple
                    target = action.move_action.target.as_tuple
                    distance = dist(pos, target)
                    if distance > 1:
                        if distance > 50:
                            last_seen = pos
                            lin = line(pos, target, self.manager.z)
                            for p in lin:
                                if self.manager.sight[p[0]*80+p[1]] and is_free(p, self.manager, True, False, False):
                                    last_seen = p
                                else:
                                    break
                            target = last_seen
                        path = None
                        if entity.id in self.assigned_paths:
                            path = self.assigned_paths[entity.id]
                            if len(path) == 0 or path[len(path) - 1] != target:
                                path = None
                                del (self.assigned_paths[entity.id])
                        if path is None and (army < 50 or entity.entity_type in consts.COMBATANTS):
                            avoid_turrets = entity.entity_type == EntityType.BUILDER_UNIT
                            through_resources = entity.entity_type in consts.COMBATANTS
                            path = pathfinding.path(self.manager, pos, target, distance * 2,
                                                    ignore_units=True, use_neighbour_point=True,
                                                    avoid_turrets=avoid_turrets, through_resources=False)
                            if not path and through_resources:
                                path = pathfinding.path(self.manager, pos, target, distance * 2,
                                                        ignore_units=True, use_neighbour_point=True,
                                                        avoid_turrets=avoid_turrets, through_resources=True)
                            if path:
                                path = path[1:]

                        if path and len(path) > 0:
                            node = path[0]
                            self.assigned_paths[entity.id] = path[1:]
                            cell = self.manager.map[node[0]][node[1]]
                            ea = self.action.entity_actions
                            if cell is None or (cell.is_unit and (cell.id not in ea or ea[cell.id].move_action)) \
                               or cell.entity_type == EntityType.RESOURCE:
                                action.move_action.target = Vec2Int(*node)
                                action.move_action.break_through = True

        zatyk_assigned = set()
        for entity in self.manager.my_entities:
            if entity.id in zatyk_assigned:
                continue
            action = self.action.entity_actions[entity.id]
            if action.move_action:
                pos = entity.position.as_tuple
                target = action.move_action.target.as_tuple
                distance = dist(pos, target)
                if distance == 1:
                    cell = self.manager.map[target[0]][target[1]]
                    ea = self.action.entity_actions
                    if cell and cell.is_unit and cell.id in ea:
                        target_action = ea[cell.id]
                        if target_action.move_action:
                            target_target = target_action.move_action.target.as_tuple
                            if target_target == pos:
                                # zatyk
                                if entity.entity_type == EntityType.BUILDER_UNIT:
                                    move = entity
                                else:
                                    move = cell

                                neighbours = [n for n in self.manager.graph[move.position.x][move.position.y]
                                              if self.manager.map[n[0]][n[1]] is None]
                                if len(neighbours):
                                    run_pos = min(neighbours, key=lambda n: self.manager.max_damage[n[0]*80+n[1]])
                                    self.action.entity_actions[move.id] = EntityAction(
                                        move_action=MoveAction(Vec2Int(*run_pos), False, False)
                                    )
                                    zatyk_assigned.add(entity.id)
                                    zatyk_assigned.add(cell.id)

        rangers_assigned = set()
        for turret in self.manager.enemy_entities:
            if turret.entity_type != EntityType.TURRET:
                continue
            cells = cells_in_range(turret.as_square(self.manager.ep), 7, player_view.map_size)
            my_rangers = []
            for cx, cy in cells:
                cell = self.manager.map[cx][cy]
                if cell and cell.player_id == player_view.my_id and cell.entity_type == EntityType.RANGED_UNIT \
                        and cell.id not in rangers_assigned:
                    my_rangers.append(cell)
                    rangers_assigned.add(cell.id)
            if (len(my_rangers) * 5) >= (turret.health / 2):
                for ranger in my_rangers:
                    self.action.entity_actions[ranger.id] = EntityAction(
                        move_action=MoveAction(turret.position, True, True),
                        attack_action=AttackAction(None, AutoAttack(7, []))
                    )

        res_to_break = set()
        for entity in self.manager.my_entities:
            action = self.action.entity_actions[entity.id]
            if action.move_action and dist(entity.position.as_tuple, action.move_action.target.as_tuple) == 1:
                cell = self.manager.map[action.move_action.target.x][action.move_action.target.y]
                if cell and cell.entity_type == EntityType.RESOURCE:
                    res_to_break.add(cell)

        for res in res_to_break:
            cells = cells_in_range(res.as_square(player_view.entity_properties), 5)
            for cx, cy in cells:
                cell = self.manager.map[cx][cy]
                if cell and cell.entity_type == EntityType.RANGED_UNIT and cell.player_id == player_view.my_id:
                    action = self.action.entity_actions[cell.id]
                    if not action.attack_action:
                        self.action.entity_actions[cell.id] = EntityAction(
                            attack_action=AttackAction(res.id, None)
                        )

        # if debug_interface:
        #     for x in range(80):
        #         for y in range(80):
        #             k = self.manager.turret_range[x*80+y]
        #             if k > 0:
        #                 debug_interface.send(Add(Primitives([
        #                     ColoredVertex(Vec2Float(x, y),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                     ColoredVertex(Vec2Float(x, y + 1),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                     ColoredVertex(Vec2Float(x + 1, y + 1),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                     ColoredVertex(Vec2Float(x + 1, y + 1),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                     ColoredVertex(Vec2Float(x + 1, y),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                     ColoredVertex(Vec2Float(x, y),
        #                                   Vec2Float(0, 0), Color(0, 1, 0, k/10)),
        #                 ], PrimitiveType.TRIANGLES)))
        #
        #     for x, y in consts.HOUSES:
        #         debug_interface.send(Add(Primitives([
        #             ColoredVertex(Vec2Float(x, y),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(x, y + 3),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(x + 3, y + 3),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(x + 3, y + 3),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(x + 3, y),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(x, y),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #         ], PrimitiveType.TRIANGLES)))
        #
        #     for bid, square in self.assigned_as_builders.items():
        #         builder = self.manager.entities[bid]
        #         left, bottom, size = square
        #
        #         debug_interface.send(Add(Primitives([
        #             ColoredVertex(Vec2Float(builder.position.x + 0.5, builder.position.y + 0.5),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 1)),
        #             ColoredVertex(Vec2Float(left + size / 2, bottom + size / 2),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 1)),
        #         ], PrimitiveType.LINES)))
        #
        #         debug_interface.send(Add(Primitives([
        #             ColoredVertex(Vec2Float(left, bottom),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(left, bottom + size),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(left + size, bottom + size),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(left + size, bottom + size),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(left + size, bottom),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #             ColoredVertex(Vec2Float(left, bottom),
        #                           Vec2Float(0, 0), Color(0, 1, 0, 0.3)),
        #         ], PrimitiveType.TRIANGLES)))
        #
        #     vertices = []
        #     max_k = max(self.manager.max_damage)
        #     if max_k:
        #         for i in range(self.manager.z):
        #             for j in range(self.manager.z):
        #                 k = (self.manager.max_damage[i*80 + j] - self.manager.pos_to_attack_for_rangers[i*80 + j]) / max_k
        #                 vertices.append(ColoredVertex(Vec2Float(i, j), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #                 vertices.append(ColoredVertex(Vec2Float(i+1, j), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #                 vertices.append(ColoredVertex(Vec2Float(i+1, j+1), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #                 vertices.append(ColoredVertex(Vec2Float(i+1, j+1), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #                 vertices.append(ColoredVertex(Vec2Float(i, j+1), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #                 vertices.append(ColoredVertex(Vec2Float(i, j), Vec2Float(0, 0), Color(1, 0, 0, k)))
        #         debug_interface.send(Add(Primitives(vertices, PrimitiveType.TRIANGLES)))

        return self.action

    def debug_update(self, player_view: PlayerView, debug_interface: DebugInterface):
        if not self.manager:
            return

        # debug_interface.send(Clear())
        # bid = next((b for b in self.assigned_as_builders), default=None)
        # if bid:
        #     builder = self.manager.entities[bid]
        #     state = debug_interface.get_state()
        #     mouse_pos = (floor(state.mouse_pos_world.x), floor(state.mouse_pos_world.y))
        #     color = GREEN if pathfinding.distance(self.manager, builder.position.as_tuple, mouse_pos, 80*80) else RED
        #     debug_interface.send(Add(Primitives([
        #         ColoredVertex(Vec2Float(mouse_pos[0] + 0.5, mouse_pos[1] + 0.5), Vec2Float(0, 0), color),
        #         ColoredVertex(Vec2Float(builder.position.x + 0.5, builder.position.y + 0.5), Vec2Float(0, 0), color),
        #     ], PrimitiveType.LINES)))
        #     path = pathfinding.path(self.manager, builder.position.as_tuple, mouse_pos, 80*80)
        #     if path:
        #         vertices = []
        #         for p in path:
        #             x, y = p
        #             vertices += [
        #                 ColoredVertex(Vec2Float(x, y), Vec2Float(0, 0), YELLOW),
        #                 ColoredVertex(Vec2Float(x+1, y), Vec2Float(0, 0), YELLOW),
        #                 ColoredVertex(Vec2Float(x+1, y+1), Vec2Float(0, 0), YELLOW),
        #                 ColoredVertex(Vec2Float(x+1, y+1), Vec2Float(0, 0), YELLOW),
        #                 ColoredVertex(Vec2Float(x, y+1), Vec2Float(0, 0), YELLOW),
        #                 ColoredVertex(Vec2Float(x, y), Vec2Float(0, 0), YELLOW),
        #             ]
        #         debug_interface.send(Add(Primitives(vertices, PrimitiveType.TRIANGLES)))
