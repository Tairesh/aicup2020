from typing import Tuple, List, Set

NEIGHBOURS_OFFSET4 = ((0, 1), (0, -1), (1, 0), (-1, 0))

INSIDE = 0
LEFT = 1
RIGHT = 2
BOTTOM = 4
TOP = 8


def abs(a: int) -> int:
    if a < 0:
        return -a
    return a


def min(a: int, b: int) -> int:
    if a < b:
        return a
    return b


def max(a: int, b: int) -> int:
    if a > b:
        return a
    return b


def point_code(square: Tuple[int, int, int], point: Tuple[int, int]):
    left, bottom, size = square
    top, right = bottom + size - 1, left + size - 1
    x, y = point

    code = INSIDE
    if x < left:
        code |= LEFT
    elif x > right:
        code |= RIGHT
    if y < bottom:
        code |= BOTTOM
    elif y > top:
        code |= TOP

    return code


def line(start: Tuple[int, int], end: Tuple[int, int], map_size=80) -> List[Tuple[int, int]]:
    """Bresenham's Line Algorithm
    Produces a list of tuples from start and end
    :param start:
    :param end:
    :return:
    """
    # Setup initial conditions
    x1, y1 = start
    x2, y2 = end
    dx = x2 - x1
    dy = y2 - y1

    # Determine how steep the line is
    is_steep = abs(dy) > abs(dx)

    # Rotate line
    if is_steep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2

    # Swap start and end points if necessary and store swap state
    swapped = False
    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
        swapped = True

    # Recalculate differentials
    dx = x2 - x1
    dy = y2 - y1

    # Calculate error
    error = int(dx / 2.0)
    ystep = 1 if y1 < y2 else -1

    # Iterate over bounding box generating points between start and end
    y = y1
    points = []
    for x in range(x1, x2 + 1):
        coord = (y, x) if is_steep else (x, y)
        if check_in_map(*coord, map_size=map_size):
            points.append(coord)
        error -= abs(dy)
        if error < 0:
            y += ystep
            error += dx

    # Reverse the list if the coordinates were swapped
    if swapped:
        points.reverse()
    return points


def cells_between(point1: Tuple[int, int], point2: Tuple[int, int], map_size: int = 80) -> List[Tuple[int, int]]:
    """List of cells that in rectangle, between these points

    :param point1:
    :param point2:
    :param map_size:
    :return:
    """
    # Setup initial conditions
    ax, ay = point1
    bx, by = point2
    min_x = ax if ax < bx else bx
    max_x = bx if ax < bx else ax
    min_y = ay if ay < by else by
    max_y = by if ay < by else ay

    # Iterate over box generating points between point1 and point2
    cells = []
    x = min_x
    while x <= max_x:
        y = min_y
        while y <= max_y:
            if check_in_map(x, y, map_size):
                cells.append((x, y))
            y += 1
        x += 1
    return cells


def check_in_map(i: int, j: int, map_size: int = 80) -> bool:
    return 0 <= i < map_size and 0 <= j < map_size


def cell_neighbours(point: Tuple[int, int], map_size: int = 80) -> Set[Tuple[int, int]]:
    """cells around

    :param point:
    :param map_size:
    :return:
    """
    x, y = point
    if x == 0 and y == 0:
        return {(1, 0), (0, 1)}
    elif x == map_size - 1 and y == map_size - 1:
        return {(map_size - 2, map_size - 1), (map_size - 1, map_size - 2)}
    elif x == 0 and y == map_size - 1:
        return {(1, map_size - 1), (0, map_size - 2)}
    elif x == map_size - 1 and y == 0:
        return {(map_size - 1, 1), (map_size - 2, 0)}
    elif x == 0:
        return {(x, y + 1), (1, y), (x, y - 1)}
    elif x == map_size - 1:
        return {(x, y + 1), (x - 1, y), (x, y - 1)}
    elif y == 0:
        return {(x + 1, y), (x, 1), (x - 1, y)}
    elif y == map_size - 1:
        return {(x - 1, y), (x, y - 1), (x + 1, y)}
    else:
        return {(x, y + 1), (x, y - 1), (x + 1, y), (x - 1, y)}


square_neighbours_cache = {}


def square_neighbours(square: Tuple[int, int, int], map_size: int = 80) -> Set[Tuple[int, int]]:
    if square not in square_neighbours_cache:
        square_neighbours_cache[square] = _square_neighbours(square, map_size)
    return square_neighbours_cache[square]


def _square_neighbours(square: Tuple[int, int, int], map_size: int = 80) -> Set[Tuple[int, int]]:
    """cells around square

    :param square:
    :param map_size:
    :return:
    """
    left, bottom, size = square
    top, right = bottom + size - 1, left + size - 1

    return set(filter(lambda p: check_in_map(p[0], p[1], map_size),
                      [(left - 1, y) for y in range(bottom, top + 1)]
                      + [(right + 1, y) for y in range(bottom, top + 1)]
                      + [(x, bottom - 1) for x in range(left, right + 1)]
                      + [(x, top + 1) for x in range(left, right + 1)]))


def sum(point1: Tuple[int, int], point2: Tuple[int, int]) -> Tuple[int, int]:
    """Sum of two points/vectors

    :param point1:
    :param point2:
    :return:
    """
    x1, y1 = point1
    x2, y2 = point2
    return x1 + x2, y1 + y2


def sub(point1: Tuple[int, int], point2: Tuple[int, int]) -> Tuple[int, int]:
    """Sub point2 from point1
    creates a vector from point1 to point2
    :param point1:
    :param point2:
    :return:
    """
    x1, y1 = point1
    x2, y2 = point2
    return x1 - x2, y1 - y2


def square_dist(point1: Tuple[int, int], point2: Tuple[int, int]) -> int:
    """hypot^2 between two points

    :param point1:
    :param point2:
    :return:
    """
    x1, y1 = point1
    x2, y2 = point2
    return (x1 - x2) ** 2 + (y1 - y2) ** 2


def manhattan_dist(point1: Tuple[int, int], point2: Tuple[int, int]) -> int:
    """Manhattan distance between two points

    :param point1:
    :param point2:
    :return:
    """
    x1, y1 = point1
    x2, y2 = point2
    return abs(x1 - x2) + abs(y1 - y2)


def manhattan_dist_to_square(point: Tuple[int, int], square: Tuple[int, int, int]) -> int:
    """Manhattan distance between point and rectangle

    :param point:
    :param square:
    :return:
    """
    px, py = point
    left, bottom, s = square
    top, right = bottom + s - 1, left + s - 1

    if left <= px <= right and bottom <= py <= top:
        return 0

    if py > top:
        h = py - top
    else:
        h = bottom - py
    if px > right:
        w = px - right
    else:
        w = left - px

    return w + h


def negate(point: Tuple[int, int]) -> Tuple[int, int]:
    """Negate vector

    :param point:
    :return:
    """
    x, y = point
    return -x, -y


def intersect_rectangles(rect1: Tuple[int, int, int, int], rect2: Tuple[int, int, int, int]) -> bool:
    left1, bottom1, w1, h1 = rect1
    top1, right1 = bottom1 + h1 - 1, left1 + w1 - 1
    left2, bottom2, w2, h2 = rect2
    top2, right2 = bottom2 + h2 - 1, left2 + w2 - 1

    # If one rectangle is on left side of other
    if left1 > right2 or left2 > right1:
        return False
    # If one rectangle is above other
    if top1 < bottom2 or top2 < bottom1:
        return False
    return True


def squares_around_point(point: Tuple[int, int], square_size: int) -> Set[Tuple[int, int, int]]:
    x, y = point
    points = set()
    for i in range(square_size):
        points.add((x - square_size, y - i, square_size))
        points.add((x + 1, y - i, square_size))
        points.add((x - i, y + 1, square_size))
        points.add((x - i, y - square_size, square_size))

    return points


ranges_cache = {}


def cells_in_range(square: Tuple[int, int, int], radius: int, map_size: int = 80) -> Set[Tuple]:
    x, y, s = square
    key = (x, y, s, radius)

    if key not in ranges_cache:
        ranges_cache[key] = _cells_in_range(square, radius, map_size)
    return ranges_cache[key]


def _cells_in_range(square: Tuple[int, int, int], radius: int, map_size: int = 80) -> Set[Tuple[int, int]]:
    x, y, s = square

    if radius == 1:
        return square_neighbours(square, map_size) | {(x, y), }
    if radius == 2 and s == 1:
        return set([p for p in (
            (x, y), (x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1), (x + 1, y + 1), (x - 1, y + 1), (x + 1, y - 1),
            (x - 1, y - 1), (x + 2, y), (x - 2, y), (x, y + 2), (x, y - 2),
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 5 and s == 1:
        return set([p for p in (
            (x, y), (x - 1, y), (x, y - 1), (x, y + 1), (x + 1, y), (x - 2, y), (x - 1, y - 1), (x - 1, y + 1),
            (x, y - 2), (x, y + 2), (x + 1, y - 1), (x + 1, y + 1), (x + 2, y), (x - 3, y), (x - 2, y - 1),
            (x - 2, y + 1), (x - 1, y - 2), (x - 1, y + 2), (x, y - 3), (x, y + 3), (x + 1, y - 2), (x + 1, y + 2),
            (x + 2, y - 1), (x + 2, y + 1), (x + 3, y), (x - 4, y), (x - 3, y - 1), (x - 3, y + 1), (x - 2, y - 2),
            (x - 2, y + 2), (x - 1, y - 3), (x - 1, y + 3), (x, y - 4), (x, y + 4), (x + 1, y - 3), (x + 1, y + 3),
            (x + 2, y - 2), (x + 2, y + 2), (x + 2, y + 1), (x + 3, y + 1), (x + 4, y + 1), (x - 5, y), (x - 4, y - 1),
            (x - 4, y + 1), (x - 3, y - 2), (x - 3, y + 2), (x - 2, y - 3), (x - 2, y + 3), (x - 1, y - 4),
            (x - 1, y + 4), (x, y - 5), (x, y + 5), (x + 1, y - 4), (x + 1, y + 4), (x + 2, y - 3), (x + 2, y + 3),
            (x + 3, y - 2), (x + 3, y + 2), (x + 4, y - 1), (x + 4, y + 1), (x + 5, y), (x + 3, y - 1), (x + 4, y)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 5 and s == 2:
        return set([p for p in (
            (x, y), (x - 1, y), (x, y - 1), (x, y + 1), (x + 1, y), (x - 2, y), (x - 1, y - 1), (x - 1, y + 1),
            (x, y - 2), (x, y + 2), (x + 1, y - 1), (x + 1, y + 1), (x + 2, y), (x - 3, y), (x - 2, y - 1),
            (x - 2, y + 1), (x - 1, y - 2), (x - 1, y + 2), (x, y - 3), (x, y + 3), (x + 1, y - 2), (x + 1, y + 2),
            (x + 2, y - 1), (x + 2, y + 1), (x + 3, y), (x - 4, y), (x - 3, y - 1), (x - 3, y + 1), (x - 2, y - 2),
            (x - 2, y + 2), (x - 1, y - 3), (x - 1, y + 3), (x, y - 4), (x, y + 4), (x + 1, y - 3), (x + 1, y + 3),
            (x + 2, y - 2), (x + 2, y + 2), (x + 2, y + 1), (x + 3, y + 1), (x + 4, y + 1), (x - 5, y), (x - 4, y - 1),
            (x - 4, y + 1), (x - 3, y - 2), (x - 3, y + 2), (x - 2, y - 3), (x - 2, y + 3), (x - 1, y - 4),
            (x - 1, y + 4), (x, y - 5), (x, y + 5), (x + 1, y - 4), (x + 1, y + 4), (x + 2, y - 3), (x + 2, y + 3),
            (x + 3, y - 2), (x + 3, y + 2), (x + 4, y - 1), (x + 4, y + 1), (x + 5, y), (x + 3, y - 1), (x + 4, y),
            (x + 6, y), (x + 6, y + 1), (x - 5, y + 1), (x - 4, y + 2), (x - 3, y + 3), (x - 2, y + 4), (x - 1, y + 5),
            (x, y + 6), (x + 1, y - 5), (x + 1, y + 5), (x + 2, y - 4), (x + 2, y + 4), (x + 3, y - 3), (x + 3, y + 3),
            (x + 4, y - 2), (x + 4, y + 2), (x + 5, y - 1), (x + 5, y + 1), (x + 1, y + 6), (x + 2, y + 5),
            (x + 3, y + 4), (x + 4, y + 3), (x + 5, y + 2)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 6 and s == 1:
        return set([p for p in (
            (x, y), (x - 1, y), (x, y - 1), (x, y + 1), (x + 1, y), (x - 2, y), (x - 1, y - 1), (x - 1, y + 1),
            (x, y - 2), (x, y + 2), (x + 1, y - 1), (x + 1, y + 1), (x + 2, y), (x - 3, y), (x - 2, y - 1),
            (x - 2, y + 1), (x - 1, y - 2), (x - 1, y + 2), (x, y - 3), (x, y + 3), (x + 1, y - 2), (x + 1, y + 2),
            (x + 2, y - 1), (x + 2, y + 1), (x + 3, y), (x - 4, y), (x - 3, y - 1), (x - 3, y + 1), (x - 2, y - 2),
            (x - 2, y + 2), (x - 1, y - 3), (x - 1, y + 3), (x, y - 4), (x, y + 4), (x + 1, y - 3), (x + 1, y + 3),
            (x + 2, y - 2), (x + 2, y + 2), (x + 3, y - 1), (x + 3, y + 1), (x + 4, y), (x - 5, y), (x - 4, y - 1),
            (x - 4, y + 1), (x - 3, y - 2), (x - 3, y + 2), (x - 2, y - 3), (x - 2, y + 3), (x - 1, y - 4),
            (x - 1, y + 4), (x, y - 5), (x, y + 5), (x + 1, y - 4), (x + 1, y + 4), (x + 2, y - 3), (x + 2, y + 3),
            (x + 3, y - 2), (x + 3, y + 2), (x + 4, y - 1), (x + 4, y + 1), (x + 5, y), (x - 6, y), (x - 5, y - 1),
            (x - 5, y + 1), (x - 4, y - 2), (x - 4, y + 2), (x - 3, y - 3), (x - 3, y + 3), (x - 2, y - 4),
            (x - 2, y + 4), (x - 1, y - 5), (x - 1, y + 5), (x, y - 6), (x, y + 6), (x + 1, y - 5), (x + 1, y + 5),
            (x + 2, y - 4), (x + 2, y + 4), (x + 3, y - 3), (x + 3, y + 3), (x + 4, y - 2), (x + 4, y + 2),
            (x + 5, y - 1), (x + 5, y + 1), (x + 6, y)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 6 and s == 2:
        return set([p for p in (
            (x, y), (x, y + 1), (x + 1, y), (x + 1, y + 1), (x - 1, y), (x - 1, y + 1), (x, y - 1), (x, y + 2),
            (x + 1, y - 1), (x + 1, y + 2), (x + 2, y), (x + 2, y + 1), (x - 2, y), (x - 2, y + 1), (x - 1, y - 1),
            (x - 1, y + 2), (x, y - 2), (x, y + 3), (x + 1, y - 2), (x + 1, y + 3), (x + 2, y - 1), (x + 2, y + 2),
            (x + 3, y), (x + 3, y + 1), (x - 3, y), (x - 3, y + 1), (x - 2, y - 1), (x - 2, y + 2), (x - 1, y - 2),
            (x - 1, y + 3), (x, y - 3), (x, y + 4), (x + 1, y - 3), (x + 1, y + 4), (x + 2, y - 2), (x + 2, y + 3),
            (x + 3, y - 1), (x + 3, y + 2), (x + 4, y), (x + 4, y + 1), (x - 4, y), (x - 4, y + 1), (x - 3, y - 1),
            (x - 3, y + 2), (x - 2, y - 2), (x - 2, y + 3), (x - 1, y - 3), (x - 1, y + 4), (x, y - 4), (x, y + 5),
            (x + 1, y - 4), (x + 1, y + 5), (x + 2, y - 3), (x + 2, y + 4), (x + 3, y - 2), (x + 3, y + 3),
            (x + 4, y - 1), (x + 4, y + 2), (x + 5, y), (x + 5, y + 1), (x - 5, y), (x - 5, y + 1), (x - 4, y - 1),
            (x - 4, y + 2), (x - 3, y - 2), (x - 3, y + 3), (x - 2, y - 3), (x - 2, y + 4), (x - 1, y - 4),
            (x - 1, y + 5), (x, y - 5), (x, y + 6), (x + 1, y - 5), (x + 1, y + 6), (x + 2, y - 4), (x + 2, y + 5),
            (x + 3, y - 3), (x + 3, y + 4), (x + 4, y - 2), (x + 4, y + 3), (x + 5, y - 1), (x + 5, y + 2), (x + 6, y),
            (x + 6, y + 1), (x - 6, y), (x - 6, y + 1), (x - 5, y - 1), (x - 5, y + 2), (x - 4, y - 2), (x - 4, y + 3),
            (x - 3, y - 3), (x - 3, y + 4), (x - 2, y - 4), (x - 2, y + 5), (x - 1, y - 5), (x - 1, y + 6), (x, y - 6),
            (x, y + 7), (x + 1, y - 6), (x + 1, y + 7), (x + 2, y - 5), (x + 2, y + 6), (x + 3, y - 4), (x + 3, y + 5),
            (x + 4, y - 3), (x + 4, y + 4), (x + 5, y - 2), (x + 5, y + 3), (x + 6, y - 1), (x + 6, y + 2), (x + 7, y),
            (x + 7, y + 1)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 10 and s == 1:
        return set([p for p in (
            (x - 10, y), (x - 9, y - 1), (x - 9, y), (x - 9, y + 1), (x - 8, y - 2), (x - 8, y - 1), (x - 8, y),
            (x - 8, y + 1), (x - 8, y + 2), (x - 7, y - 3), (x - 7, y - 2), (x - 7, y - 1), (x - 7, y), (x - 7, y + 1),
            (x - 7, y + 2), (x - 7, y + 3), (x - 6, y - 4), (x - 6, y - 3), (x - 6, y - 2), (x - 6, y - 1), (x - 6, y),
            (x - 6, y + 1), (x - 6, y + 2), (x - 6, y + 3), (x - 6, y + 4), (x - 5, y - 5), (x - 5, y - 4),
            (x - 5, y - 3), (x - 5, y - 2), (x - 5, y - 1), (x - 5, y), (x - 5, y + 1), (x - 5, y + 2), (x - 5, y + 3),
            (x - 5, y + 4), (x - 5, y + 5), (x - 4, y - 6), (x - 4, y - 5), (x - 4, y - 4), (x - 4, y - 3),
            (x - 4, y - 2), (x - 4, y - 1), (x - 4, y), (x - 4, y + 1), (x - 4, y + 2), (x - 4, y + 3), (x - 4, y + 4),
            (x - 4, y + 5), (x - 4, y + 6), (x - 3, y - 7), (x - 3, y - 6), (x - 3, y - 5), (x - 3, y - 4),
            (x - 3, y - 3), (x - 3, y - 2), (x - 3, y - 1), (x - 3, y), (x - 3, y + 1), (x - 3, y + 2), (x - 3, y + 3),
            (x - 3, y + 4), (x - 3, y + 5), (x - 3, y + 6), (x - 3, y + 7), (x - 2, y - 8), (x - 2, y - 7),
            (x - 2, y - 6), (x - 2, y - 5), (x - 2, y - 4), (x - 2, y - 3), (x - 2, y - 2), (x - 2, y - 1), (x - 2, y),
            (x - 2, y + 1), (x - 2, y + 2), (x - 2, y + 3), (x - 2, y + 4), (x - 2, y + 5), (x - 2, y + 6),
            (x - 2, y + 7), (x - 2, y + 8), (x - 1, y - 9), (x - 1, y - 8), (x - 1, y - 7), (x - 1, y - 6),
            (x - 1, y - 5), (x - 1, y - 4), (x - 1, y - 3), (x - 1, y - 2), (x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
            (x - 1, y + 2), (x - 1, y + 3), (x - 1, y + 4), (x - 1, y + 5), (x - 1, y + 6), (x - 1, y + 7),
            (x - 1, y + 8), (x - 1, y + 9), (x, y - 10), (x, y - 9), (x, y - 8), (x, y - 7), (x, y - 6), (x, y - 5),
            (x, y - 4), (x, y - 3), (x, y - 2), (x, y - 1), (x, y), (x, y + 1), (x, y + 2), (x, y + 3), (x, y + 4),
            (x, y + 5), (x, y + 6), (x, y + 7), (x, y + 8), (x, y + 9), (x, y + 10), (x + 1, y - 9), (x + 1, y - 8),
            (x + 1, y - 7), (x + 1, y - 6), (x + 1, y - 5), (x + 1, y - 4), (x + 1, y - 3), (x + 1, y - 2),
            (x + 1, y - 1), (x + 1, y), (x + 1, y + 1), (x + 1, y + 2), (x + 1, y + 3), (x + 1, y + 4), (x + 1, y + 5),
            (x + 1, y + 6), (x + 1, y + 7), (x + 1, y + 8), (x + 1, y + 9), (x + 2, y - 8), (x + 2, y - 7),
            (x + 2, y - 6), (x + 2, y - 5), (x + 2, y - 4), (x + 2, y - 3), (x + 2, y - 2), (x + 2, y - 1), (x + 2, y),
            (x + 2, y + 1), (x + 2, y + 2), (x + 2, y + 3), (x + 2, y + 4), (x + 2, y + 5), (x + 2, y + 6),
            (x + 2, y + 7), (x + 2, y + 8), (x + 3, y - 7), (x + 3, y - 6), (x + 3, y - 5), (x + 3, y - 4),
            (x + 3, y - 3), (x + 3, y - 2), (x + 3, y - 1), (x + 3, y), (x + 3, y + 1), (x + 3, y + 2), (x + 3, y + 3),
            (x + 3, y + 4), (x + 3, y + 5), (x + 3, y + 6), (x + 3, y + 7), (x + 4, y - 6), (x + 4, y - 5),
            (x + 4, y - 4), (x + 4, y - 3), (x + 4, y - 2), (x + 4, y - 1), (x + 4, y), (x + 4, y + 1), (x + 4, y + 2),
            (x + 4, y + 3), (x + 4, y + 4), (x + 4, y + 5), (x + 4, y + 6), (x + 5, y - 5), (x + 5, y - 4),
            (x + 5, y - 3), (x + 5, y - 2), (x + 5, y - 1), (x + 5, y), (x + 5, y + 1), (x + 5, y + 2), (x + 5, y + 3),
            (x + 5, y + 4), (x + 5, y + 5), (x + 6, y - 4), (x + 6, y - 3), (x + 6, y - 2), (x + 6, y - 1), (x + 6, y),
            (x + 6, y + 1), (x + 6, y + 2), (x + 6, y + 3), (x + 6, y + 4), (x + 7, y - 3), (x + 7, y - 2),
            (x + 7, y - 1), (x + 7, y), (x + 7, y + 1), (x + 7, y + 2), (x + 7, y + 3), (x + 8, y - 2), (x + 8, y - 1),
            (x + 8, y), (x + 8, y + 1), (x + 8, y + 2), (x + 9, y - 1), (x + 9, y), (x + 9, y + 1), (x + 10, y)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 5 and s == 5:
        return set([p for p in (
            (x - 5, y), (x - 5, y + 4), (x - 4, y - 1), (x - 4, y), (x - 4, y + 1), (x - 4, y + 3), (x - 4, y + 4),
            (x - 4, y + 5), (x - 3, y - 2), (x - 3, y - 1), (x - 3, y), (x - 3, y + 1), (x - 3, y + 2), (x - 3, y + 3),
            (x - 3, y + 4), (x - 3, y + 5), (x - 3, y + 6), (x - 2, y - 3), (x - 2, y - 2), (x - 2, y - 1), (x - 2, y),
            (x - 2, y + 1), (x - 2, y + 2), (x - 2, y + 3), (x - 2, y + 4), (x - 2, y + 5), (x - 2, y + 6),
            (x - 2, y + 7), (x - 1, y - 4), (x - 1, y - 3), (x - 1, y - 2), (x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
            (x - 1, y + 2), (x - 1, y + 3), (x - 1, y + 4), (x - 1, y + 5), (x - 1, y + 6), (x - 1, y + 7),
            (x - 1, y + 8), (x, y - 5), (x, y - 4), (x, y - 3), (x, y - 2), (x, y - 1), (x, y), (x, y + 1), (x, y + 2),
            (x, y + 3), (x, y + 4), (x, y + 5), (x, y + 6), (x, y + 7), (x, y + 8), (x, y + 9), (x + 1, y - 4),
            (x + 1, y - 3), (x + 1, y - 2), (x + 1, y - 1), (x + 1, y), (x + 1, y + 1), (x + 1, y + 2), (x + 1, y + 3),
            (x + 1, y + 4), (x + 1, y + 5), (x + 1, y + 6), (x + 1, y + 7), (x + 1, y + 8), (x + 2, y - 3),
            (x + 2, y - 2), (x + 2, y - 1), (x + 2, y), (x + 2, y + 1), (x + 2, y + 2), (x + 2, y + 3), (x + 2, y + 4),
            (x + 2, y + 5), (x + 2, y + 6), (x + 2, y + 7), (x + 3, y - 4), (x + 3, y - 3), (x + 3, y - 2),
            (x + 3, y - 1), (x + 3, y), (x + 3, y + 1), (x + 3, y + 2), (x + 3, y + 3), (x + 3, y + 4), (x + 3, y + 5),
            (x + 3, y + 6), (x + 3, y + 7), (x + 3, y + 8), (x + 4, y - 5), (x + 4, y - 4), (x + 4, y - 3),
            (x + 4, y - 2), (x + 4, y - 1), (x + 4, y), (x + 4, y + 1), (x + 4, y + 2), (x + 4, y + 3), (x + 4, y + 4),
            (x + 4, y + 5), (x + 4, y + 6), (x + 4, y + 7), (x + 4, y + 8), (x + 4, y + 9), (x + 5, y - 4),
            (x + 5, y - 3), (x + 5, y - 2), (x + 5, y - 1), (x + 5, y), (x + 5, y + 1), (x + 5, y + 2), (x + 5, y + 3),
            (x + 5, y + 4), (x + 5, y + 5), (x + 5, y + 6), (x + 5, y + 7), (x + 5, y + 8), (x + 6, y - 3),
            (x + 6, y - 2), (x + 6, y - 1), (x + 6, y), (x + 6, y + 1), (x + 6, y + 2), (x + 6, y + 3), (x + 6, y + 4),
            (x + 6, y + 5), (x + 6, y + 6), (x + 6, y + 7), (x + 7, y - 2), (x + 7, y - 1), (x + 7, y), (x + 7, y + 1),
            (x + 7, y + 2), (x + 7, y + 3), (x + 7, y + 4), (x + 7, y + 5), (x + 7, y + 6), (x + 8, y - 1), (x + 8, y),
            (x + 8, y + 1), (x + 8, y + 3), (x + 8, y + 4), (x + 8, y + 5), (x + 9, y), (x + 9, y + 4)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 5 and s == 3:
        return set([p for p in (
            (x - 5, y), (x - 5, y + 2), (x - 4, y - 1), (x - 4, y), (x - 4, y + 1), (x - 4, y + 2), (x - 4, y + 3),
            (x - 3, y - 2), (x - 3, y - 1), (x - 3, y), (x - 3, y + 1), (x - 3, y + 2), (x - 3, y + 3), (x - 3, y + 4),
            (x - 2, y - 3), (x - 2, y - 2), (x - 2, y - 1), (x - 2, y), (x - 2, y + 1), (x - 2, y + 2), (x - 2, y + 3),
            (x - 2, y + 4), (x - 2, y + 5), (x - 1, y - 4), (x - 1, y - 3), (x - 1, y - 2), (x - 1, y - 1), (x - 1, y),
            (x - 1, y + 1), (x - 1, y + 2), (x - 1, y + 3), (x - 1, y + 4), (x - 1, y + 5), (x - 1, y + 6), (x, y - 5),
            (x, y - 4), (x, y - 3), (x, y - 2), (x, y - 1), (x, y), (x, y + 1), (x, y + 2), (x, y + 3), (x, y + 4),
            (x, y + 5), (x, y + 6), (x, y + 7), (x + 1, y - 4), (x + 1, y - 3), (x + 1, y - 2), (x + 1, y - 1),
            (x + 1, y), (x + 1, y + 1), (x + 1, y + 2), (x + 1, y + 3), (x + 1, y + 4), (x + 1, y + 5), (x + 1, y + 6),
            (x + 2, y - 5), (x + 2, y - 4), (x + 2, y - 3), (x + 2, y - 2), (x + 2, y - 1), (x + 2, y), (x + 2, y + 1),
            (x + 2, y + 2), (x + 2, y + 3), (x + 2, y + 4), (x + 2, y + 5), (x + 2, y + 6), (x + 2, y + 7),
            (x + 3, y - 4), (x + 3, y - 3), (x + 3, y - 2), (x + 3, y - 1), (x + 3, y), (x + 3, y + 1), (x + 3, y + 2),
            (x + 3, y + 3), (x + 3, y + 4), (x + 3, y + 5), (x + 3, y + 6), (x + 4, y - 3), (x + 4, y - 2),
            (x + 4, y - 1), (x + 4, y), (x + 4, y + 1), (x + 4, y + 2), (x + 4, y + 3), (x + 4, y + 4), (x + 4, y + 5),
            (x + 5, y - 2), (x + 5, y - 1), (x + 5, y), (x + 5, y + 1), (x + 5, y + 2), (x + 5, y + 3), (x + 5, y + 4),
            (x + 6, y - 1), (x + 6, y), (x + 6, y + 1), (x + 6, y + 2), (x + 6, y + 3), (x + 7, y), (x + 7, y + 2)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 7 and s == 2:
        return set([p for p in (
            (x - 7, y), (x - 7, y + 1), (x - 6, y - 1), (x - 6, y), (x - 6, y + 1), (x - 6, y + 2), (x - 5, y - 2),
            (x - 5, y - 1), (x - 5, y), (x - 5, y + 1), (x - 5, y + 2), (x - 5, y + 3), (x - 4, y - 3), (x - 4, y - 2),
            (x - 4, y - 1), (x - 4, y), (x - 4, y + 1), (x - 4, y + 2), (x - 4, y + 3), (x - 4, y + 4), (x - 3, y - 4),
            (x - 3, y - 3), (x - 3, y - 2), (x - 3, y - 1), (x - 3, y), (x - 3, y + 1), (x - 3, y + 2), (x - 3, y + 3),
            (x - 3, y + 4), (x - 3, y + 5), (x - 2, y - 5), (x - 2, y - 4), (x - 2, y - 3), (x - 2, y - 2),
            (x - 2, y - 1), (x - 2, y), (x - 2, y + 1), (x - 2, y + 2), (x - 2, y + 3), (x - 2, y + 4), (x - 2, y + 5),
            (x - 2, y + 6), (x - 1, y - 6), (x - 1, y - 5), (x - 1, y - 4), (x - 1, y - 3), (x - 1, y - 2),
            (x - 1, y - 1), (x - 1, y), (x - 1, y + 1), (x - 1, y + 2), (x - 1, y + 3), (x - 1, y + 4), (x - 1, y + 5),
            (x - 1, y + 6), (x - 1, y + 7), (x, y - 7), (x, y - 6), (x, y - 5), (x, y - 4), (x, y - 3), (x, y - 2),
            (x, y - 1), (x, y), (x, y + 1), (x, y + 2), (x, y + 3), (x, y + 4), (x, y + 5), (x, y + 6), (x, y + 7),
            (x, y + 8), (x + 1, y - 7), (x + 1, y - 6), (x + 1, y - 5), (x + 1, y - 4), (x + 1, y - 3), (x + 1, y - 2),
            (x + 1, y - 1), (x + 1, y), (x + 1, y + 1), (x + 1, y + 2), (x + 1, y + 3), (x + 1, y + 4), (x + 1, y + 5),
            (x + 1, y + 6), (x + 1, y + 7), (x + 1, y + 8), (x + 1, y + 9), (x + 2, y - 6), (x + 2, y - 5),
            (x + 2, y - 4), (x + 2, y - 3), (x + 2, y - 2), (x + 2, y - 1), (x + 2, y), (x + 2, y + 1), (x + 2, y + 2),
            (x + 2, y + 3), (x + 2, y + 4), (x + 2, y + 5), (x + 2, y + 6), (x + 2, y + 7), (x + 3, y - 5),
            (x + 3, y - 4), (x + 3, y - 3), (x + 3, y - 2), (x + 3, y - 1), (x + 3, y), (x + 3, y + 1), (x + 3, y + 2),
            (x + 3, y + 3), (x + 3, y + 4), (x + 3, y + 5), (x + 3, y + 6), (x + 4, y - 4), (x + 4, y - 3),
            (x + 4, y - 2), (x + 4, y - 1), (x + 4, y), (x + 4, y + 1), (x + 4, y + 2), (x + 4, y + 3), (x + 4, y + 4),
            (x + 4, y + 5), (x + 5, y - 3), (x + 5, y - 2), (x + 5, y - 1), (x + 5, y), (x + 5, y + 1), (x + 5, y + 2),
            (x + 5, y + 3), (x + 5, y + 4), (x + 6, y - 2), (x + 6, y - 1), (x + 6, y), (x + 6, y + 1), (x + 6, y + 2),
            (x + 6, y + 3), (x + 7, y - 1), (x + 7, y), (x + 7, y + 1), (x + 7, y + 2), (x + 8, y), (x + 8, y + 1),
            (x + 9, y + 1)
        ) if check_in_map(p[0], p[1], map_size)])
    if radius == 10 and s == 2:
        return set([p for p in (
            (x - 10, y), (x - 10, y + 1), (x - 9, y - 1), (x - 9, y), (x - 9, y + 1), (x - 9, y + 2), (x - 8, y - 2),
            (x - 8, y - 1), (x - 8, y), (x - 8, y + 1), (x - 8, y + 2), (x - 8, y + 3), (x - 7, y - 3), (x - 7, y - 2),
            (x - 7, y - 1), (x - 7, y), (x - 7, y + 1), (x - 7, y + 2), (x - 7, y + 3), (x - 7, y + 4), (x - 6, y - 4),
            (x - 6, y - 3), (x - 6, y - 2), (x - 6, y - 1), (x - 6, y), (x - 6, y + 1), (x - 6, y + 2), (x - 6, y + 3),
            (x - 6, y + 4), (x - 6, y + 5), (x - 5, y - 5), (x - 5, y - 4), (x - 5, y - 3), (x - 5, y - 2),
            (x - 5, y - 1), (x - 5, y), (x - 5, y + 1), (x - 5, y + 2), (x - 5, y + 3), (x - 5, y + 4), (x - 5, y + 5),
            (x - 5, y + 6), (x - 4, y - 6), (x - 4, y - 5), (x - 4, y - 4), (x - 4, y - 3), (x - 4, y - 2),
            (x - 4, y - 1), (x - 4, y), (x - 4, y + 1), (x - 4, y + 2), (x - 4, y + 3), (x - 4, y + 4), (x - 4, y + 5),
            (x - 4, y + 6), (x - 4, y + 7), (x - 3, y - 7), (x - 3, y - 6), (x - 3, y - 5), (x - 3, y - 4),
            (x - 3, y - 3), (x - 3, y - 2), (x - 3, y - 1), (x - 3, y), (x - 3, y + 1), (x - 3, y + 2), (x - 3, y + 3),
            (x - 3, y + 4), (x - 3, y + 5), (x - 3, y + 6), (x - 3, y + 7), (x - 3, y + 8), (x - 2, y - 8),
            (x - 2, y - 7), (x - 2, y - 6), (x - 2, y - 5), (x - 2, y - 4), (x - 2, y - 3), (x - 2, y - 2),
            (x - 2, y - 1), (x - 2, y), (x - 2, y + 1), (x - 2, y + 2), (x - 2, y + 3), (x - 2, y + 4), (x - 2, y + 5),
            (x - 2, y + 6), (x - 2, y + 7), (x - 2, y + 8), (x - 2, y + 9), (x - 1, y - 9), (x - 1, y - 8),
            (x - 1, y - 7), (x - 1, y - 6), (x - 1, y - 5), (x - 1, y - 4), (x - 1, y - 3), (x - 1, y - 2),
            (x - 1, y - 1), (x - 1, y), (x - 1, y + 1), (x - 1, y + 2), (x - 1, y + 3), (x - 1, y + 4), (x - 1, y + 5),
            (x - 1, y + 6), (x - 1, y + 7), (x - 1, y + 8), (x - 1, y + 9), (x - 1, y + 10), (x, y - 10), (x, y - 9),
            (x, y - 8), (x, y - 7), (x, y - 6), (x, y - 5), (x, y - 4), (x, y - 3), (x, y - 2), (x, y - 1), (x, y),
            (x, y + 1), (x, y + 2), (x, y + 3), (x, y + 4), (x, y + 5), (x, y + 6), (x, y + 7), (x, y + 8), (x, y + 9),
            (x, y + 10), (x, y + 11), (x + 1, y - 10), (x + 1, y - 9), (x + 1, y - 8), (x + 1, y - 7), (x + 1, y - 6),
            (x + 1, y - 5), (x + 1, y - 4), (x + 1, y - 3), (x + 1, y - 2), (x + 1, y - 1), (x + 1, y), (x + 1, y + 1),
            (x + 1, y + 2), (x + 1, y + 3), (x + 1, y + 4), (x + 1, y + 5), (x + 1, y + 6), (x + 1, y + 7),
            (x + 1, y + 8), (x + 1, y + 9), (x + 1, y + 10), (x + 1, y + 11), (x + 2, y - 9), (x + 2, y - 8),
            (x + 2, y - 7), (x + 2, y - 6), (x + 2, y - 5), (x + 2, y - 4), (x + 2, y - 3), (x + 2, y - 2),
            (x + 2, y - 1), (x + 2, y), (x + 2, y + 1), (x + 2, y + 2), (x + 2, y + 3), (x + 2, y + 4), (x + 2, y + 5),
            (x + 2, y + 6), (x + 2, y + 7), (x + 2, y + 8), (x + 2, y + 9), (x + 2, y + 10), (x + 3, y - 8),
            (x + 3, y - 7), (x + 3, y - 6), (x + 3, y - 5), (x + 3, y - 4), (x + 3, y - 3), (x + 3, y - 2),
            (x + 3, y - 1), (x + 3, y), (x + 3, y + 1), (x + 3, y + 2), (x + 3, y + 3), (x + 3, y + 4), (x + 3, y + 5),
            (x + 3, y + 6), (x + 3, y + 7), (x + 3, y + 8), (x + 3, y + 9), (x + 4, y - 7), (x + 4, y - 6),
            (x + 4, y - 5), (x + 4, y - 4), (x + 4, y - 3), (x + 4, y - 2), (x + 4, y - 1), (x + 4, y), (x + 4, y + 1),
            (x + 4, y + 2), (x + 4, y + 3), (x + 4, y + 4), (x + 4, y + 5), (x + 4, y + 6), (x + 4, y + 7),
            (x + 4, y + 8), (x + 5, y - 6), (x + 5, y - 5), (x + 5, y - 4), (x + 5, y - 3), (x + 5, y - 2),
            (x + 5, y - 1), (x + 5, y), (x + 5, y + 1), (x + 5, y + 2), (x + 5, y + 3), (x + 5, y + 4), (x + 5, y + 5),
            (x + 5, y + 6), (x + 5, y + 7), (x + 6, y - 5), (x + 6, y - 4), (x + 6, y - 3), (x + 6, y - 2),
            (x + 6, y - 1), (x + 6, y), (x + 6, y + 1), (x + 6, y + 2), (x + 6, y + 3), (x + 6, y + 4), (x + 6, y + 5),
            (x + 6, y + 6), (x + 7, y - 4), (x + 7, y - 3), (x + 7, y - 2), (x + 7, y - 1), (x + 7, y), (x + 7, y + 1),
            (x + 7, y + 2), (x + 7, y + 3), (x + 7, y + 4), (x + 7, y + 5), (x + 8, y - 3), (x + 8, y - 2),
            (x + 8, y - 1), (x + 8, y), (x + 8, y + 1), (x + 8, y + 2), (x + 8, y + 3), (x + 8, y + 4), (x + 9, y - 2),
            (x + 9, y - 1), (x + 9, y), (x + 9, y + 1), (x + 9, y + 2), (x + 9, y + 3), (x + 10, y - 1), (x + 10, y),
            (x + 10, y + 1), (x + 10, y + 2), (x + 11, y), (x + 11, y + 1)
        ) if check_in_map(p[0], p[1], map_size)])

    print(f"something went wrong with size {s} and radius {radius}")

    cells = cells_between((x - radius, y - radius), (x + s + radius, y + s + radius), map_size)
    return set(filter(
        lambda p: check_in_map(*p, map_size=map_size) and manhattan_dist_to_square(p, square) <= radius,
        cells
    ))
