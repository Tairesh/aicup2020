import time


def check_time(name, func, args):
    start_time = time.time()
    results = {}
    for a in args:
        results[a] = func(*a)
    end_time = time.time()
    print(f"{name}: {end_time - start_time} sec")
    return results
