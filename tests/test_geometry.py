import unittest

import geometry


class TestGeometry(unittest.TestCase):

    def test_bresenham(self):
        points1 = geometry.line((0, 0), (3, 4))
        self.assertEqual([(0, 0), (1, 1), (1, 2), (2, 3), (3, 4)], points1)
        points2 = geometry.line((3, 4), (0, 0))
        self.assertEqual(set(points1), set(points2))
        self.assertEqual(points1, list(reversed(points2)))

        self.assertEqual([(0, 0)], geometry.line((0, 0), (0, 0)))

    def test_cells_between(self):
        points1 = geometry.cells_between((0, 0), (3, 4))
        self.assertEqual([(0, 0), (0, 1), (0, 2), (0, 3), (0, 4),
                          (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
                          (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
                          (3, 0), (3, 1), (3, 2), (3, 3), (3, 4)], points1)
        points2 = geometry.cells_between((3, 4), (0, 0))
        self.assertEqual(points1, points2)

    def test_cells_neighbours(self):
        self.assertEqual({(4, 4), (2, 4), (3, 3), (3, 5)}, geometry.cell_neighbours((3, 4)))
        self.assertEqual({(1, 0), (0, 1)}, geometry.cell_neighbours((0, 0)))

    def test_square_neighbours(self):
        self.assertEqual({
            (0, 1), (0, 2), (0, 3), (0, 4), (0, 5),
            (6, 1), (6, 2), (6, 3), (6, 4), (6, 5),
            (1, 0), (2, 0), (3, 0), (4, 0), (5, 0),
            (1, 6), (2, 6), (3, 6), (4, 6), (5, 6),
        }, geometry.square_neighbours((1, 1, 5)))
        self.assertEqual({(2, 0), (2, 1), (0, 2), (1, 2)}, geometry.square_neighbours((0, 0, 2)))

    def test_sum(self):
        self.assertEqual((4, 6), geometry.sum((1, 2), (3, 4)))

    def test_sub(self):
        self.assertEqual((-2, -2), geometry.sub((1, 2), (3, 4)))

    def test_square_dist(self):
        self.assertEqual(8, geometry.square_dist((1, 2), (3, 4)))

    def test_manhattan_dist(self):
        self.assertEqual(4, geometry.manhattan_dist((1, 2), (3, 4)))

    def test_negate(self):
        self.assertEqual((-3, -4), geometry.negate((3, 4)))

    def test_intercept_rectangles(self):
        rect1 = (0, 0, 4, 5)
        rect2 = (1, 1, 1, 1)
        rect3 = (3, 4, 2, 2)
        self.assertTrue(geometry.intersect_rectangles(rect1, rect2))
        self.assertTrue(geometry.intersect_rectangles(rect1, rect3))
        self.assertFalse(geometry.intersect_rectangles(rect2, rect3))

    def test_manhattan_dist_to_square(self):
        rect = (0, 0, 4)
        point1 = (0, 0)
        point2 = (4, 5)
        self.assertEqual(0, geometry.manhattan_dist_to_square(point1, rect))
        self.assertEqual(3, geometry.manhattan_dist_to_square(point2, rect))

    def test_points_computing(self):
        square = (15, 15, 7)
        self.assertEqual(geometry.LEFT, geometry.point_code(square, (5, 15)))
        self.assertEqual(geometry.INSIDE, geometry.point_code(square, (15, 15)))
        self.assertEqual(geometry.RIGHT, geometry.point_code(square, (25, 17)))
        self.assertEqual(geometry.LEFT | geometry.TOP, geometry.point_code(square, (5, 25)))

    def test_diamond(self):
        self.assertEqual({
            (0, 6), (1, 5), (1, 6), (1, 7), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (3, 3), (3, 4), (3, 5), (3, 6),
            (3, 7), (3, 8), (3, 9), (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (5, 1),
            (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (6, 0), (6, 1), (6, 2),
            (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (7, 1), (7, 2), (7, 3),
            (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (8, 2), (8, 3), (8, 4), (8, 5), (8, 6),
            (8, 7), (8, 8), (8, 9), (8, 10), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (10, 4), (10, 5),
            (10, 6), (10, 7), (10, 8), (11, 5), (11, 6), (11, 7), (12, 6)},
            geometry.cells_in_range((6, 6, 1), 6))
        self.assertEqual({
            (0, 6), (0, 7), (1, 5), (1, 6), (1, 7), (1, 8), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (3, 3),
            (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7),
            (4, 8), (4, 9), (4, 10), (4, 11), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9),
            (5, 10), (5, 11), (5, 12), (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9),
            (6, 10), (6, 11), (6, 12), (6, 13), (7, 0), (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8),
            (7, 9), (7, 10), (7, 11), (7, 12), (7, 13), (8, 1), (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8),
            (8, 9), (8, 10), (8, 11), (8, 12), (9, 2), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10),
            (9, 11), (10, 3), (10, 4), (10, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10), (11, 4), (11, 5), (11, 6),
            (11, 7), (11, 8), (11, 9), (12, 5), (12, 6), (12, 7), (12, 8), (13, 6), (13, 7)},
            geometry.cells_in_range((6, 6, 2), 6))
        self.assertEqual({
            (0, 5), (1, 4), (1, 5), (1, 6), (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (3, 2), (3, 3), (3, 4), (3, 5),
            (3, 6), (3, 7), (3, 8), (4, 1), (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (5, 0),
            (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (6, 1), (6, 2), (6, 3),
            (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8),
            (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (9, 4), (9, 5), (9, 6), (10, 5)},
            geometry.cells_in_range((5, 5, 1), 5))
        self.assertEqual({
            (5, 5), (4, 5), (5, 4), (5, 6), (6, 5), (3, 5), (4, 4), (4, 6), (5, 3), (5, 7), (6, 4), (6, 6), (7, 5),
            (2, 5), (3, 4), (3, 6), (4, 3), (4, 7), (5, 2), (5, 8), (6, 3), (6, 7), (7, 4), (7, 6), (8, 5), (1, 5),
            (2, 4), (2, 6), (3, 3), (3, 7), (4, 2), (4, 8), (5, 1), (5, 9), (6, 2), (6, 8), (7, 3), (7, 7), (8, 4),
            (8, 6), (9, 5), (0, 5), (1, 4), (1, 6), (2, 3), (2, 7), (3, 2), (3, 8), (4, 1), (4, 9), (5, 0), (5, 10),
            (6, 1), (6, 9), (7, 2), (7, 8), (8, 3), (8, 7), (9, 4), (9, 6), (10, 5), (0, 6), (1, 7), (2, 8), (3, 9),
            (4, 10), (5, 11), (6, 0), (6, 10), (7, 1), (7, 9), (8, 2), (8, 8), (9, 3), (9, 7), (10, 4), (10, 6),
            (11, 5), (6, 11), (7, 10), (8, 9), (9, 8), (10, 7), (11, 6)
        }, geometry.cells_in_range((5, 5, 2), 5))
        self.assertEqual({(4, 5), (5, 4), (5, 5), (5, 6), (6, 5)}, geometry.cells_in_range((5, 5, 1), 1))
        self.assertEqual({
            (1, 11), (2, 10), (2, 11), (2, 12), (3, 9), (3, 10), (3, 11), (3, 12), (3, 13), (4, 8), (4, 9), (4, 10),
            (4, 11), (4, 12), (4, 13), (4, 14), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12), (5, 13), (5, 14),
            (5, 15), (6, 6), (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13), (6, 14), (6, 15), (6, 16),
            (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12), (7, 13), (7, 14), (7, 15), (7, 16),
            (7, 17), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13), (8, 14),
            (8, 15), (8, 16), (8, 17), (8, 18), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10),
            (9, 11), (9, 12), (9, 13), (9, 14), (9, 15), (9, 16), (9, 17), (9, 18), (9, 19), (10, 2), (10, 3), (10, 4),
            (10, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10), (10, 11), (10, 12), (10, 13), (10, 14), (10, 15),
            (10, 16), (10, 17), (10, 18), (10, 19), (10, 20), (11, 1), (11, 2), (11, 3), (11, 4), (11, 5), (11, 6),
            (11, 7), (11, 8), (11, 9), (11, 10), (11, 11), (11, 12), (11, 13), (11, 14), (11, 15), (11, 16), (11, 17),
            (11, 18), (11, 19), (11, 20), (11, 21), (12, 2), (12, 3), (12, 4), (12, 5), (12, 6), (12, 7), (12, 8),
            (12, 9), (12, 10), (12, 11), (12, 12), (12, 13), (12, 14), (12, 15), (12, 16), (12, 17), (12, 18),
            (12, 19), (12, 20), (13, 3), (13, 4), (13, 5), (13, 6), (13, 7), (13, 8), (13, 9), (13, 10), (13, 11),
            (13, 12), (13, 13), (13, 14), (13, 15), (13, 16), (13, 17), (13, 18), (13, 19), (14, 4), (14, 5), (14, 6),
            (14, 7), (14, 8), (14, 9), (14, 10), (14, 11), (14, 12), (14, 13), (14, 14), (14, 15), (14, 16), (14, 17),
            (14, 18), (15, 5), (15, 6), (15, 7), (15, 8), (15, 9), (15, 10), (15, 11), (15, 12), (15, 13), (15, 14),
            (15, 15), (15, 16), (15, 17), (16, 6), (16, 7), (16, 8), (16, 9), (16, 10), (16, 11), (16, 12), (16, 13),
            (16, 14), (16, 15), (16, 16), (17, 7), (17, 8), (17, 9), (17, 10), (17, 11), (17, 12), (17, 13), (17, 14),
            (17, 15), (18, 8), (18, 9), (18, 10), (18, 11), (18, 12), (18, 13), (18, 14), (19, 9), (19, 10), (19, 11),
            (19, 12), (19, 13), (20, 10), (20, 11), (20, 12), (21, 11)},
            geometry.cells_in_range((11, 11, 1), 10))
        self.assertEqual(
            {(5, 10), (5, 14), (6, 9), (6, 10), (6, 11), (6, 13), (6, 14), (6, 15), (7, 8), (7, 9), (7, 10), (7, 11),
             (7, 12), (7, 13), (7, 14), (7, 15), (7, 16), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13),
             (8, 14), (8, 15), (8, 16), (8, 17), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10), (9, 11), (9, 12), (9, 13),
             (9, 14), (9, 15), (9, 16), (9, 17), (9, 18), (10, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10),
             (10, 11), (10, 12), (10, 13), (10, 14), (10, 15), (10, 16), (10, 17), (10, 18), (10, 19), (11, 6), (11, 7),
             (11, 8), (11, 9), (11, 10), (11, 11), (11, 12), (11, 13), (11, 14), (11, 15), (11, 16), (11, 17), (11, 18),
             (12, 7), (12, 8), (12, 9), (12, 10), (12, 11), (12, 12), (12, 13), (12, 14), (12, 15), (12, 16), (12, 17),
             (13, 6), (13, 7), (13, 8), (13, 9), (13, 10), (13, 11), (13, 12), (13, 13), (13, 14), (13, 15), (13, 16),
             (13, 17), (13, 18), (14, 5), (14, 6), (14, 7), (14, 8), (14, 9), (14, 10), (14, 11), (14, 12), (14, 13),
             (14, 14), (14, 15), (14, 16), (14, 17), (14, 18), (14, 19), (15, 6), (15, 7), (15, 8), (15, 9), (15, 10),
             (15, 11), (15, 12), (15, 13), (15, 14), (15, 15), (15, 16), (15, 17), (15, 18), (16, 7), (16, 8), (16, 9),
             (16, 10), (16, 11), (16, 12), (16, 13), (16, 14), (16, 15), (16, 16), (16, 17), (17, 8), (17, 9), (17, 10),
             (17, 11), (17, 12), (17, 13), (17, 14), (17, 15), (17, 16), (18, 9), (18, 10), (18, 11), (18, 13),
             (18, 14), (18, 15), (19, 10), (19, 14)},
            geometry.cells_in_range((10, 10, 5), 5))
        self.assertEqual(
            {(5, 10), (5, 12), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12),
             (7, 13), (7, 14), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13), (8, 14), (8, 15), (9, 6),
             (9, 7), (9, 8), (9, 9), (9, 10), (9, 11), (9, 12), (9, 13), (9, 14), (9, 15), (9, 16), (10, 5), (10, 6),
             (10, 7), (10, 8), (10, 9), (10, 10), (10, 11), (10, 12), (10, 13), (10, 14), (10, 15), (10, 16), (10, 17),
             (11, 6), (11, 7), (11, 8), (11, 9), (11, 10), (11, 11), (11, 12), (11, 13), (11, 14), (11, 15), (11, 16),
             (12, 5), (12, 6), (12, 7), (12, 8), (12, 9), (12, 10), (12, 11), (12, 12), (12, 13), (12, 14), (12, 15),
             (12, 16), (12, 17), (13, 6), (13, 7), (13, 8), (13, 9), (13, 10), (13, 11), (13, 12), (13, 13), (13, 14),
             (13, 15), (13, 16), (14, 7), (14, 8), (14, 9), (14, 10), (14, 11), (14, 12), (14, 13), (14, 14), (14, 15),
             (15, 8), (15, 9), (15, 10), (15, 11), (15, 12), (15, 13), (15, 14), (16, 9), (16, 10), (16, 11), (16, 12),
             (16, 13), (17, 10), (17, 12)},
            geometry.cells_in_range((10, 10, 3), 5))
        self.assertEqual(
            {(0, 10), (0, 11), (1, 9), (1, 10), (1, 11), (1, 12), (2, 8), (2, 9), (2, 10), (2, 11), (2, 12), (2, 13),
             (3, 7), (3, 8), (3, 9), (3, 10), (3, 11), (3, 12), (3, 13), (3, 14), (4, 6), (4, 7), (4, 8), (4, 9),
             (4, 10), (4, 11), (4, 12), (4, 13), (4, 14), (4, 15), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
             (5, 11), (5, 12), (5, 13), (5, 14), (5, 15), (5, 16), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9),
             (6, 10), (6, 11), (6, 12), (6, 13), (6, 14), (6, 15), (6, 16), (6, 17), (7, 3), (7, 4), (7, 5), (7, 6),
             (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12), (7, 13), (7, 14), (7, 15), (7, 16), (7, 17), (7, 18),
             (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13),
             (8, 14), (8, 15), (8, 16), (8, 17), (8, 18), (8, 19), (9, 1), (9, 2), (9, 3), (9, 4), (9, 5), (9, 6),
             (9, 7), (9, 8), (9, 9), (9, 10), (9, 11), (9, 12), (9, 13), (9, 14), (9, 15), (9, 16), (9, 17), (9, 18),
             (9, 19), (9, 20), (10, 0), (10, 1), (10, 2), (10, 3), (10, 4), (10, 5), (10, 6), (10, 7), (10, 8), (10, 9),
             (10, 10), (10, 11), (10, 12), (10, 13), (10, 14), (10, 15), (10, 16), (10, 17), (10, 18), (10, 19),
             (10, 20), (10, 21), (11, 0), (11, 1), (11, 2), (11, 3), (11, 4), (11, 5), (11, 6), (11, 7), (11, 8),
             (11, 9), (11, 10), (11, 11), (11, 12), (11, 13), (11, 14), (11, 15), (11, 16), (11, 17), (11, 18),
             (11, 19), (11, 20), (11, 21), (12, 1), (12, 2), (12, 3), (12, 4), (12, 5), (12, 6), (12, 7), (12, 8),
             (12, 9), (12, 10), (12, 11), (12, 12), (12, 13), (12, 14), (12, 15), (12, 16), (12, 17), (12, 18),
             (12, 19), (12, 20), (13, 2), (13, 3), (13, 4), (13, 5), (13, 6), (13, 7), (13, 8), (13, 9), (13, 10),
             (13, 11), (13, 12), (13, 13), (13, 14), (13, 15), (13, 16), (13, 17), (13, 18), (13, 19), (14, 3), (14, 4),
             (14, 5), (14, 6), (14, 7), (14, 8), (14, 9), (14, 10), (14, 11), (14, 12), (14, 13), (14, 14), (14, 15),
             (14, 16), (14, 17), (14, 18), (15, 4), (15, 5), (15, 6), (15, 7), (15, 8), (15, 9), (15, 10), (15, 11),
             (15, 12), (15, 13), (15, 14), (15, 15), (15, 16), (15, 17), (16, 5), (16, 6), (16, 7), (16, 8), (16, 9),
             (16, 10), (16, 11), (16, 12), (16, 13), (16, 14), (16, 15), (16, 16), (17, 6), (17, 7), (17, 8), (17, 9),
             (17, 10), (17, 11), (17, 12), (17, 13), (17, 14), (17, 15), (18, 7), (18, 8), (18, 9), (18, 10), (18, 11),
             (18, 12), (18, 13), (18, 14), (19, 8), (19, 9), (19, 10), (19, 11), (19, 12), (19, 13), (20, 9), (20, 10),
             (20, 11), (20, 12), (21, 10), (21, 11)}, geometry.cells_in_range((10, 10, 2), 10))
        self.assertEqual(
            {(3, 10), (3, 11), (4, 9), (4, 10), (4, 11), (4, 12), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12), (5, 13),
             (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13), (6, 14), (7, 6), (7, 7), (7, 8), (7, 9),
             (7, 10), (7, 11), (7, 12), (7, 13), (7, 14), (7, 15), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10),
             (8, 11), (8, 12), (8, 13), (8, 14), (8, 15), (8, 16), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9),
             (9, 10), (9, 11), (9, 12), (9, 13), (9, 14), (9, 15), (9, 16), (9, 17), (10, 3), (10, 4), (10, 5), (10, 6),
             (10, 7), (10, 8), (10, 9), (10, 10), (10, 11), (10, 12), (10, 13), (10, 14), (10, 15), (10, 16), (10, 17),
             (10, 18), (11, 3), (11, 4), (11, 5), (11, 6), (11, 7), (11, 8), (11, 9), (11, 10), (11, 11), (11, 12),
             (11, 13), (11, 14), (11, 15), (11, 16), (11, 17), (11, 18), (11, 19), (12, 4), (12, 5), (12, 6), (12, 7),
             (12, 8), (12, 9), (12, 10), (12, 11), (12, 12), (12, 13), (12, 14), (12, 15), (12, 16), (12, 17), (13, 5),
             (13, 6), (13, 7), (13, 8), (13, 9), (13, 10), (13, 11), (13, 12), (13, 13), (13, 14), (13, 15), (13, 16),
             (14, 6), (14, 7), (14, 8), (14, 9), (14, 10), (14, 11), (14, 12), (14, 13), (14, 14), (14, 15), (15, 7),
             (15, 8), (15, 9), (15, 10), (15, 11), (15, 12), (15, 13), (15, 14), (16, 8), (16, 9), (16, 10), (16, 11),
             (16, 12), (16, 13), (17, 9), (17, 10), (17, 11), (17, 12), (18, 10), (18, 11), (19, 11)},
            geometry.cells_in_range((10, 10, 2), 7))
