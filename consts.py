from model.attack_action import AttackAction
from model.auto_attack import AutoAttack
from model.entity_type import EntityType

VALID_TARGETS_FOR_BUILDER = [
    EntityType.RESOURCE, EntityType.HOUSE, EntityType.MELEE_BASE, EntityType.RANGED_BASE, EntityType.BUILDER_BASE,
    EntityType.WALL, EntityType.TURRET, EntityType.MELEE_UNIT, EntityType.RANGED_UNIT, EntityType.BUILDER_UNIT,
]
VALID_TARGETS_FOR_COMBATANTS = []

COMBATANTS = {EntityType.MELEE_UNIT, EntityType.RANGED_UNIT}
NONCOMBATANS = {EntityType.BUILDER_UNIT}

BASE_ENTITIES = {EntityType.HOUSE, EntityType.MELEE_BASE, EntityType.RANGED_BASE, EntityType.BUILDER_BASE,
                 EntityType.BUILDER_UNIT, EntityType.TURRET}
BASE_OFFSET = 3
SABOTEURS_OFFSET = 10
SABOTEURS = {EntityType.MELEE_UNIT, EntityType.RANGED_UNIT, EntityType.BUILDER_UNIT}
COEFF_MINERS_TO_RES = 3

ATTACK_ACTION_BUILDERS = AttackAction(None, AutoAttack(10, VALID_TARGETS_FOR_BUILDER))

UNIT_TYPES = {EntityType.MELEE_UNIT, EntityType.RANGED_UNIT, EntityType.BUILDER_UNIT}

# HOUSES = [(i, 2) for i in range(6, 45, 4)] + [(2, i) for i in range(6, 45, 4)]
# HOUSES += [(i, 6) for i in range(10, 45, 4)] + [(6, i) for i in range(10, 45, 4)]
# for i in range(10, 45, 4):
#     for j in range(10, 45, 4):
#         HOUSES.append((i, j))
#
# HOUSES = sorted(set(HOUSES), key=lambda p: p[0] + p[1])
# print(HOUSES)
HOUSES = [(6, 2), (2, 6), (10, 2), (2, 10), (14, 2), (2, 14), (10, 6), (6, 10), (18, 2), (2, 18), (14, 6),
          (6, 14), (10, 10), (22, 2), (2, 22), (18, 6), (6, 18), (10, 14), (14, 10), (26, 2), (2, 26), (22, 6), (6, 22),
          (10, 18), (14, 14), (18, 10), (30, 2), (2, 30), (26, 6), (6, 26), (10, 22), (14, 18), (18, 14), (22, 10),
          (34, 2), (2, 34), (30, 6), (6, 30), (10, 26), (14, 22), (18, 18), (22, 14), (26, 10), (38, 2), (2, 38),
          (34, 6), (6, 34), (10, 30), (14, 26), (18, 22), (22, 18), (26, 14), (30, 10), (42, 2), (2, 42), (38, 6),
          (6, 38), (10, 34), (14, 30), (18, 26), (22, 22), (26, 18), (30, 14), (34, 10), (42, 6), (6, 42), (10, 38),
          (14, 34), (18, 30), (22, 26), (26, 22), (30, 18), (34, 14), (38, 10), (10, 42), (14, 38), (18, 34), (22, 30),
          (26, 26), (30, 22), (34, 18), (38, 14), (42, 10), (14, 42), (18, 38), (22, 34), (26, 30), (30, 26), (34, 22),
          (38, 18), (42, 14), (18, 42), (22, 38), (26, 34), (30, 30), (34, 26), (38, 22), (42, 18), (22, 42), (26, 38),
          (30, 34), (34, 30), (38, 26), (42, 22), (26, 42), (30, 38), (34, 34), (38, 30), (42, 26), (30, 42), (34, 38),
          (38, 34), (42, 30), (34, 42), (38, 38), (42, 34), (38, 42), (42, 38), (42, 42)]
