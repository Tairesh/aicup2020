from typing import Set, Tuple, Dict, Optional, List

from collections import deque

import consts
import pathfinding
from geometry import manhattan_dist as dist, manhattan_dist_to_square as dist_to_square, cells_in_range
from model.entity import Entity
from model.entity_type import EntityType
from model.player import Player
from model.player_view import PlayerView
from utils import is_free


class Manager:

    def __init__(self, player_view: PlayerView):
        self.player_view: PlayerView = player_view
        self.me: Player = next(filter(lambda p: p.id == self.player_view.my_id, self.player_view.players))

        # never updates
        self.z = player_view.map_size
        self.ep = player_view.entity_properties
        self.f = player_view.fog_of_war
        self.end = player_view.max_tick_count
        self.graph = pathfinding.create_graph(self.z)
        if self.f:
            self.round = 3 if len(player_view.players) == 2 else 2
        else:
            self.round = 1

        # updates frequently
        self.miners_targets = [
            (10, self.z - 6),
            (self.z // 3, self.z - 6),
            (int(2 * self.z / 3), self.z - 6),
            (self.z - 6, self.z - 6),
            (self.z - 6, int(2 * self.z / 3)),
            (self.z - 6, self.z // 3),
            (self.z - 6, 10),
        ]
        self.player2gone = False
        self.player3gone = len(player_view.players) == 2
        self.player4gone = len(player_view.players) == 2
        self.seen_any_turret = False
        self.houses_updated = False

        # updates every tick
        self.tick = player_view.current_tick
        self.resources_count = 0
        self.food_provided = 0
        self.food_provided_potential = 0
        self.food_used = 0
        self.potential_resources = 0

        self.entities: Dict[int, Entity] = {}
        self.resources: Set[Entity] = set()
        self.resources_claimed: Set[int] = set()
        self.my_entities: Set[Entity] = set()
        self.enemy_entities: Set[Entity] = set()
        self.enemy_buildings: Dict[int, Set[Entity]] = {}
        self.enemy_units: Dict[int, Set[Entity]] = {}
        for p in player_view.players:
            if p.id == player_view.my_id:
                continue
            self.enemy_buildings[p.id] = set()
            self.enemy_units[p.id] = set()
        self.my_combatants: Set[Entity] = set()
        self.my_builders: Set[Entity] = set()
        self.my_buildings: Dict[EntityType, Set[Entity]] = {
            EntityType.BUILDER_BASE: set(),
            EntityType.RANGED_BASE: set(),
            EntityType.MELEE_BASE: set(),
            EntityType.TURRET: set(),
            EntityType.HOUSE: set(),
            EntityType.WALL: set(),
        }
        self.need_repair: Set[Entity] = set()
        self.my_base: Set[Entity] = set()
        self.saboteurs: List[Entity] = []

        self.base_corner: Tuple[int, int] = (20, 20)
        self.middle_of_army: Tuple[int, int] = (17, 17)

        self.max_damage: List[int] = [0 for _ in range(self.z ** 2)]
        self.turret_range: List[int] = [0 for _ in range(self.z ** 2)]
        self.sight: List[int] = [False for _ in range(self.z ** 2)] if self.round > 1 \
            else [True for _ in range(self.z ** 2)]

        self.map: Dict[int, Dict[int, Optional[Entity]]] = dict((i, dict((j, None) for j in range(self.z)))
                                                                for i in range(self.z))

    def tick_update(self, player_view: PlayerView):
        self.player_view = player_view
        self.me = next(filter(lambda p: p.id == self.player_view.my_id, self.player_view.players))
        for i in range(self.z):
            for j in range(self.z):
                self.map[i][j] = None

        self.tick = player_view.current_tick
        self.resources_count = 0
        self.food_provided = 0
        self.food_provided_potential = 0
        self.food_used = 0
        self.potential_resources = 0

        self.entities.clear()
        self.resources.clear()
        self.resources_claimed.clear()
        self.my_entities.clear()
        self.enemy_entities.clear()
        for p in player_view.players:
            if p.id == player_view.my_id:
                continue
            self.enemy_buildings[p.id].clear()
            self.enemy_units[p.id].clear()
        self.my_combatants.clear()
        self.my_builders.clear()
        for k in self.my_buildings:
            self.my_buildings[k].clear()
        self.need_repair.clear()
        self.my_base.clear()
        self.saboteurs.clear()
        self.max_damage = [0 for _ in range(self.z ** 2)]
        self.turret_range = [0 for _ in range(self.z ** 2)]
        if self.round > 1:
            self.sight = [False for _ in range(self.z ** 2)]
            self.sight[0] = True
            self.sight[1] = True
            self.sight[2] = True
            self.sight[80] = True
            self.sight[81] = True
            self.sight[160] = True

        base_right = 20
        base_top = 20
        army_x = 20
        army_y = 20
        for entity in self.player_view.entities:
            self.entities[entity.id] = entity
            props = self.ep[entity.entity_type]

            left, bottom, size = entity.as_square(self.ep)
            right, top = left + size - 1, bottom + size - 1
            for i in range(left, right + 1):
                for j in range(bottom, top + 1):
                    self.map[i][j] = entity

            if entity.entity_type == EntityType.RESOURCE:
                self.resources_count += 1
                self.resources.add(entity)

            elif entity.player_id == self.player_view.my_id:
                self.my_entities.add(entity)
                if self.round > 1:
                    cells = cells_in_range(entity.as_square(self.ep), props.sight_range, self.z)
                    for cx, cy in cells:
                        self.sight[cx * self.z + cy] = True

                if entity.entity_type in consts.COMBATANTS:
                    self.my_combatants.add(entity)
                    army_x += entity.position.x
                    army_y += entity.position.y

                if entity.entity_type == EntityType.BUILDER_UNIT:
                    self.my_builders.add(entity)
                    for mine in self.miners_targets:
                        if dist(mine, (left, bottom)) < self.z // 5:
                            self.miners_targets.remove(mine)

                if not props.can_move:
                    self.my_buildings[entity.entity_type].add(entity)
                    if not entity.active or entity.health < props.max_health:
                        self.need_repair.add(entity)

                if entity.active:
                    self.food_used += props.population_use
                    self.food_provided += props.population_provide
                self.food_provided_potential += props.population_provide

            else:  # enemies
                self.enemy_entities.add(entity)
                if entity.is_unit:
                    self.enemy_units[entity.player_id].add(entity)
                else:
                    self.enemy_buildings[entity.player_id].add(entity)
                    if entity.entity_type == EntityType.TURRET:
                        self.seen_any_turret = True

                if props.attack and entity.active and entity.entity_type != EntityType.BUILDER_UNIT:
                    k = 1 if entity.is_unit else 0
                    cells = cells_in_range(entity.as_square(self.ep), props.attack.attack_range + k, self.z)
                    for cell in cells:
                        cx, cy = cell
                        self.max_damage[cx*self.z+cy] += props.attack.damage

                    if entity.entity_type == EntityType.TURRET:
                        cells = cells_in_range(entity.as_square(self.ep),
                                               self.ep[EntityType.TURRET].attack.attack_range, self.z)
                        for cell in cells:
                            cx, cy = cell
                            self.turret_range[cx * self.z + cy] += 1

        for entity in self.my_entities:
            left, bottom, size = entity.as_square(self.ep)
            right, top = left + size - 1, bottom + size - 1

            if entity.entity_type in consts.BASE_ENTITIES:
                self.my_base.add(entity)

            rb, tb = right + consts.BASE_OFFSET, top + consts.BASE_OFFSET
            if rb > base_right:
                base_right = rb
            if tb > base_top:
                base_top = tb

        self.base_corner = (base_right, base_top)
        if len(self.my_combatants) > 2:
            self.middle_of_army = round(army_x / len(self.my_combatants)), round(army_y / len(self.my_combatants))
        else:
            self.middle_of_army = (10, 10)

        sab_right = base_right + consts.SABOTEURS_OFFSET
        sab_top = base_top + consts.SABOTEURS_OFFSET
        for enemy in filter(
            lambda e: e.position.x < sab_right and e.position.y < sab_top and e.entity_type in consts.SABOTEURS,
            self.enemy_entities
        ):
            pos = enemy.position.as_tuple
            my_unit = min(
                self.my_base,
                key=lambda e: dist_to_square(pos, e.as_square(self.ep)),
                default=None,
            )
            if my_unit and dist_to_square(pos, my_unit.as_square(self.ep)) < 20:
                self.saboteurs.append(enemy)

        self.saboteurs.sort(key=lambda s: self._danger_factor(s), reverse=True)

        for builder in self.my_builders:
            neighbours = self.graph[builder.position.x][builder.position.y]
            for n in neighbours:
                cell = self.map[n[0]][n[1]]
                if cell and cell.entity_type == EntityType.RESOURCE:
                    self.potential_resources += 1
                    break

        p4 = (0, self.z - 1)
        p3 = (self.z - 1, 0)
        p2 = (self.z - 1, self.z - 1)
        if any((1 for e in self.my_combatants if dist(e.position.as_tuple, p3) <= 10)):
            self.player3gone = not any((1 for e in self.enemy_entities if dist(e.position.as_tuple, p3) <= 10))
        if any((1 for e in self.my_combatants if dist(e.position.as_tuple, p2) <= 10)):
            self.player2gone = not any((1 for e in self.enemy_entities if dist(e.position.as_tuple, p2) <= 10))
        if any((1 for e in self.my_combatants if dist(e.position.as_tuple, p4) <= 10)):
            self.player4gone = not any((1 for e in self.enemy_entities if dist(e.position.as_tuple, p4) <= 10))

        if not self.houses_updated and len(self.my_buildings[EntityType.RANGED_BASE]) > 0:
            consts.HOUSES.append((1, 1))
            consts.HOUSES.append((0, 0))
            consts.HOUSES.sort(key=lambda p: dist(p, (2, 2)))

    def free_resource(self, start: Tuple[int, int], limit=80) -> Optional[Tuple[int, int]]:
        queue = deque()
        visited = set()

        queue.append(start)
        visited.add(start)

        while len(queue) > 0:
            if len(queue) > limit:
                return None

            node = queue.popleft()

            for neighbour in self.graph[node[0]][node[1]]:
                cell = self.map[neighbour[0]][neighbour[1]]
                if cell and cell.entity_type == EntityType.RESOURCE and cell.id not in self.resources_claimed:
                    neighbs = self.graph[neighbour[0]][neighbour[1]]
                    if not any(1 for n in neighbs if self.map[n[0]][n[1]]
                               and self.map[n[0]][n[1]].player_id == self.me.id
                               and self.map[n[0]][n[1]].entity_type == EntityType.BUILDER_UNIT):
                        self.resources_claimed.add(cell.id)
                        return cell.position.as_tuple

                if neighbour in visited:
                    continue

                if self.max_damage[neighbour[0]*80 + neighbour[1]] > 0:
                    continue

                if not is_free(neighbour, self, False, True):
                    continue

                visited.add(neighbour)
                queue.append(neighbour)
        return None

    def _nearest_base_unit(self, pos: Tuple[int, int]):
        return min(self.my_base, key=lambda e: dist(e.position.as_tuple, pos), default=None)

    def _count_near_base_minus_combatants(self, pos: Tuple[int, int]):
        cells = cells_in_range((pos[0], pos[1], 1), 10, self.z)
        count_base = 0
        count_combatants = 0
        for c in cells:
            cell = self.map[c[0]][c[1]]
            if cell and cell.player_id == self.me.id:
                if cell.entity_type in consts.BASE_ENTITIES:
                    count_base += 1
                if cell.entity_type in consts.COMBATANTS:
                    count_combatants += 1
        return count_base - count_combatants

    def _danger_factor(self, saboteur: Entity):
        pos = saboteur.position.as_tuple
        nb = self._nearest_base_unit(pos)
        return 10 - dist(pos, nb.position.as_tuple) + self._count_near_base_minus_combatants(pos)
