from collections import defaultdict, deque
from typing import Dict, Set, Tuple, Optional, List

from geometry import cell_neighbours, check_in_map
from utils import is_free


def create_graph(z: int) -> Dict[int, Dict[int, Set[Tuple[int, int]]]]:
    graph = defaultdict(dict)

    for i in range(z):
        for j in range(z):
            graph[i][j] = cell_neighbours((i, j), z)

    return graph


def path_exists(manager, start: Tuple[int, int], end: Tuple[int, int],
                limit=30, ignore_units=False, use_neighbour_point=False, avoid_turrets=False) -> bool:
    queue = deque()
    visited = set()

    queue.append(start)
    visited.add(start)

    while len(queue) > 0:
        if len(queue) > limit:
            return False

        node = queue.popleft()
        if node == end:
            return True

        for neighbour in manager.graph[node[0]][node[1]]:
            if use_neighbour_point:
                if neighbour == end:
                    return True

            if neighbour in visited:
                continue

            if not is_free(neighbour, manager, ignore_units, avoid_turrets):
                continue

            visited.add(neighbour)
            queue.append(neighbour)

    return False


def distance(manager, start: Tuple[int, int], end: Tuple[int, int],
             limit=30, ignore_units=False, use_neighbour_point=False, avoid_turrets=False) -> Optional[int]:
    queue = deque()
    visited = defaultdict(bool)
    distances: Dict[int, int] = {}

    queue.append(start)
    visited[start[0] * 80 + start[1]] = True
    distances[start[0] * 80 + start[1]] = 0

    while len(queue) > 0:
        if len(queue) > limit:
            return None

        node = queue.popleft()
        sk = node[0] * 80 + node[1]
        if node == end:
            return distances[sk]

        for neighbour in manager.graph[node[0]][node[1]]:
            if use_neighbour_point:
                if neighbour == end:
                    return distances[sk] + 1

            nsk = neighbour[0] * 80 + neighbour[1]
            if visited[nsk]:
                continue

            if not is_free(neighbour, manager, ignore_units, avoid_turrets):
                continue

            visited[nsk] = True
            queue.append(neighbour)
            distances[nsk] = distances[sk] + 1

    return None


def path(manager, start: Tuple[int, int], end: Tuple[int, int], limit=30,
         ignore_units=False, use_neighbour_point=False, avoid_turrets=False, through_resources=False) \
        -> Optional[List[Tuple[int, int]]]:

    if not check_in_map(end[0], end[1]):
        return None

    if not is_free(end, manager, ignore_units, avoid_turrets, through_resources):
        if not use_neighbour_point:
            return None
        else:
            if not any(p for p in manager.graph[end[0]][end[1]]
                       if is_free(p, manager, ignore_units, avoid_turrets, through_resources)):
                return None

    queue = deque()
    visited = defaultdict(bool)
    prevs: Dict[int, Optional[Tuple[int, int]]] = {}

    def reconstruct_path(n: Tuple[int, int]):
        p = []
        while n is not None:
            p.append(n)
            n = prevs[n[0]*80+n[1]]
        return list(reversed(p))

    queue.append(start)
    sk = start[0]*80+start[1]
    visited[sk] = True
    prevs[sk] = None

    while len(queue) > 0:
        if len(queue) > limit:
            return None

        node = queue.popleft()
        if node == end:
            return reconstruct_path(node)

        for neighbour in manager.graph[node[0]][node[1]]:
            nsk = neighbour[0]*80+neighbour[1]
            if use_neighbour_point:
                if neighbour == end:
                    prevs[nsk] = node
                    return reconstruct_path(neighbour)

            if visited[nsk]:
                continue

            if not is_free(neighbour, manager, ignore_units, avoid_turrets, through_resources):
                continue

            visited[nsk] = True
            queue.append(neighbour)
            prevs[nsk] = node

    return None
