#!/bin/bash

./aicup2020 --config config.json &
cd strats/middle
pypy3 main.py 127.0.0.1 31002 &
cd ../right
pypy3 main.py 127.0.0.1 31003 &
cd ../left
pypy3 main.py 127.0.0.1 31004 &
sleep .5
