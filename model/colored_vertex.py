from .color import Color
from .vec2_float import Vec2Float


class ColoredVertex:
    def __init__(self, world_pos: Vec2Float, screen_offset: Vec2Float, color: Color):
        self.world_pos: Vec2Float = world_pos
        self.screen_offset: Vec2Float = screen_offset
        self.color: Color = color

    @staticmethod
    def read_from(stream):
        if stream.read_bool():
            world_pos = Vec2Float.read_from(stream)
        else:
            world_pos = None
        screen_offset = Vec2Float.read_from(stream)
        color = Color.read_from(stream)
        return ColoredVertex(world_pos, screen_offset, color)

    def write_to(self, stream):
        if self.world_pos is None:
            stream.write_bool(False)
        else:
            stream.write_bool(True)
            self.world_pos.write_to(stream)
        self.screen_offset.write_to(stream)
        self.color.write_to(stream)

    def __repr__(self):
        return "ColoredVertex(" + \
               repr(self.world_pos) + "," + \
               repr(self.screen_offset) + "," + \
               repr(self.color) + \
               ")"
