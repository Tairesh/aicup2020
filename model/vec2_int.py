from typing import Tuple, Optional

from model.vec2_float import Vec2Float


class Vec2Int:
    def __init__(self, x, y):
        self.x: int = x
        self.y: int = y

    @staticmethod
    def read_from(stream):
        x = stream.read_int()
        y = stream.read_int()
        return Vec2Int(x, y)

    def write_to(self, stream):
        stream.write_int(self.x)
        stream.write_int(self.y)

    @property
    def as_tuple(self) -> Tuple[int, int]:
        return self.x, self.y

    @property
    def as_float(self) -> Vec2Float:
        return Vec2Float(self.x, self.y)

    @staticmethod
    def from_tuple(p: Tuple[int, int, Optional[int]]):
        return Vec2Int(p[0], p[1])

    def __repr__(self):
        return "Vec2Int(" + \
               repr(self.x) + "," + \
               repr(self.y) + \
               ")"
