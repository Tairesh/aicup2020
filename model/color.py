class Color:
    def __init__(self, r, g, b, a):
        self.r: float = r
        self.g: float = g
        self.b: float = b
        self.a: float = a

    @staticmethod
    def read_from(stream):
        r = stream.read_float()
        g = stream.read_float()
        b = stream.read_float()
        a = stream.read_float()
        return Color(r, g, b, a)

    def write_to(self, stream):
        stream.write_float(self.r)
        stream.write_float(self.g)
        stream.write_float(self.b)
        stream.write_float(self.a)

    def __repr__(self):
        return "Color(" + \
               repr(self.r) + "," + \
               repr(self.g) + "," + \
               repr(self.b) + "," + \
               repr(self.a) + \
               ")"


RED = Color(1, 0, 0, 1)
GREEN = Color(0, 1, 0, 1)
BLUE = Color(0, 0, 1, 1)
YELLOW = Color(1, 1, 0, 1)
